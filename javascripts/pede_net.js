
/**
 * Éste módulo es el encargado de inicializar los controladores de los diferentes módulos que son utilizados 
 * en la configuración y ejecución de las redes ecológicas.
 *
 * @module module_pede_net
 */
var module_net = (function () {


    var _AMBIENTE = 1;
    var _TEST = false;
    var _VERBOSE = true;
    var MOD_COMUNIDAD = 1;
    var _REGION_SELECTED;
    var _REGION_TEXT_SELECTED;
    var _MODULO = "comunidad"
    var _taxones = []
    
    var _loadexternaldatafuente = false
    var _loadexternaldatadestino = false

    var _iddatafuente = null
    var _iddatadestino = null
    var _nivelesFuente = []
    var _nivelesDestino = []
    var _nivelTaxonomico = ""

    // actualizar este arreglo si cambian los ids de las secciones
    var _SCROLL_SECTIONS = ["section0","hist_comunidad","tbl_comunidad","tbl_container"];
    var _SCROLL_INDEX = 0;
    
    var _tipo_modulo = MOD_COMUNIDAD;
    
    var _map_module_net,
            _variable_module_net,
            _language_module_net,
            _res_display_module_net,
            _utils_module, _utils_module, _module_toast;

    var _url_front, _url_api, _url_comunidad, _url_auth;

    var _url_geoserver = "http://geoportal.conabio.gob.mx:80/geoserver/cnb/wms?",
            _workspace = "cnb";

    // var _toastr = toastr;
    var _iTrans;

    var _componente_fuente;
    var _componente_sumidero;

    var TIPO_FUENTE = 0, TIPO_SUMIDERO = 1;

    var _session_value;
    var _COUNTER_PETICIONES_PRECARGA = 0
    var _niveles_total = []



    /**
     * Este método inicializa el módulo de internacionalización del sistema 
     * y enlaza el método de configuración e inicialización para la generación de redes ecológicas.
     *
     * @function startModule
     * @public
     * 
     * @param {string} verbose - Bandera para desplegar modo verbose
     */
    function startModule(verbose, version) {

        _VERBOSE ? console.log("startModule") : _VERBOSE;
        _VERBOSE = verbose;

        // Se cargan los archivos de idiomas y depsues son cargados los modulos subsecuentes
        _language_module_net = language_module(_VERBOSE, version);
        _language_module_net.startLanguageModule(this, _tipo_modulo);

    }


    /**
     * Método ejecutado después de la configuración del módulo de internacionalización. Se encarga de
     * inicializar los controladores de redes, mapas, tablas, histogramas y otros componentes principales.
     *
     * @function loadModules
     * @public
     */
    function loadModules() {

        _VERBOSE ? console.log("loadModules") : _VERBOSE;

        _iTrans = _language_module_net.getI18();

        _module_toast = toast_module(_VERBOSE);
        _module_toast.startToast();

        _utils_module = utils_module();
        _utils_module.startUtilsModule();

        _session_value = _utils_module.getCookie("sessionid");
        console.log("session_value:" + _session_value)



        _map_module_net = map_module(_url_geoserver, _workspace, _VERBOSE, _url_api, _utils_module);
        _map_module_net.loadCountrySelect();
        _map_module_net.loadResolutionSelect();
        _map_module_net.loadSourceSelect();

        genLinkURL();

        // un id es enviado para diferenciar el componente del grupo de variables en caso de que sea mas de uno (caso comunidad)
        _variable_module_net = variable_module(_VERBOSE, _url_api);
        _variable_module_net.startVar(0, _language_module_net, _tipo_modulo, _map_module_net, _utils_module);

        // creación dinámica de selector de variables
        var ids_comp_variables = ['fuente', 'sumidero'];

        // el calculo de fechas es sobre el primer selector unicamente
        _componente_fuente = _variable_module_net.createSelectorComponent("div_seleccion_variables_fuente", ids_comp_variables[0], "lb_fuente", true, false, false, false, true, true);

        _componente_sumidero = _variable_module_net.createSelectorComponent("div_seleccion_variables_sumidero", ids_comp_variables[1], "lb_sumidero", true, false, false, false, true, false);

        _res_display_module_net = res_display_net_module(_VERBOSE, _url_api);
        _res_display_module_net.startResNetDisplay(_variable_module_net, _language_module_net, _map_module_net, ids_comp_variables, _tipo_modulo, _TEST);

        _map_module_net.setDisplayModule(_res_display_module_net);

        _language_module_net.addModuleForLanguage(_res_display_module_net, null, _map_module_net, _variable_module_net);

        _initializeComponents();

        verificaAuthUser()

    }


    //TODO: se desea encapsualr esta funcion para no repetir codigo en el resto de los archivos
    function verificaAuthUser(){

        console.log("verificaAuthUser")

        var actionUrl = _url_auth + "/auth/isAuth"

        if(_session_value == undefined || _session_value == ""){
            console.log("sin sessionid")
            return
        }
        // console.log(session_value)

        $.ajax({
            type: "POST",
            url: actionUrl,
            data: {
                sessionid: _session_value
            },
            success: function(resp){
              
                var session = resp.session[0]
                console.log(session)

                if(resp.status == 0 ){
                    var profile = session.sess.user
                    console.log(profile)

                    $("#loginbtn").text("Mi cuenta")
                    $("#loginbtn").attr("data-target", "")
                    $("#loginbtn").attr("href", _url_front + "/datoscargados.html")
                    
                }

                return null;

            },
            error: function(jqXHR, textStatus, errorThrown) {
                _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;
                console.log("usuario no autenticado")

                $("#loginbtn").text("Login")
                $("#loginbtn").attr("data-target", "#modalLogin")
                $("#loginbtn").attr("href", "")

            }
        });

    }


    /**
     * Inicializa y configura los diferentes componetes secundarios que son necesarios para la ejecución de un análisis de redes ecológicas.
     *
     * @function _initializeComponents
     * @private
     */
    function _initializeComponents() {

        _VERBOSE ? console.log("_initializeComponents") : _VERBOSE;

        _utils_module = utils_module();
        _utils_module.startUtilsModule();

        _module_toast.options = {
            "debug": false,
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 2000,
            "extendedTimeOut": 2000,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": true,
            "progressBar": true
        };

        _loadexternaldatafuente = false;
        _loadexternaldatadestino = false;
        obtieneIdsDatosTerceros();


        $(function () {

            // var year = parseInt(new Date().getFullYear());
            // obtnego el proximo numero divisible entre 10. 2016 -> 2020; 2017 -> 2020; 2021 -> 2030
            // year = Math.round(year / 10) * 10;

            $("#fechaini").val("");
            $("#fechafin").val("");

        });


        $("#chkFosil").click(function (event) {

            var $this = $(this);

            if ($this.is(':checked')) {

                $("#labelFosil").text("Si");

                // _regenMessage();
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_fosil_act'), "info");

            } else {

                $("#labelFosil").text("No");

                // _regenMessage();

                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_fosil_des'), "info");

            }

        });


        // checkbox que se activa cuando se desea tomar en cuanta un minimo de ocurrencias
        $("#chkFecha").click(function (event) {

            var $this = $(this);

            if ($this.is(':checked')) {
                
                // $("#sliderFecha").slider("enable");

                $("#lb_sfecha").text(_iTrans.prop('lb_si'));

                // _regenMessage();
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_chkfecha'), "info");

            } else {

                $("#lb_sfecha").text(_iTrans.prop('lb_no'));

                // _regenMessage();
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_chkfecha_des'), "info");

            }

        });

        $("#generaRed").click(function (e) {

            _VERBOSE ? console.log("generaRed") : _VERBOSE;
            _VERBOSE ? console.log(_componente_fuente.getVarSelArray()) : _VERBOSE;
            _VERBOSE ? console.log(_componente_sumidero.getVarSelArray()) : _VERBOSE;
            
            loadingsNet(1);
            
            var min_occ = parseInt($("#occ_number").val());

            _res_display_module_net.cleanLegendGroups();

            
            var analisis_level_selected_fuente = _componente_fuente.analisis_level_selected
            var analisis_level_selected_sumidero = _componente_sumidero.analisis_level_selected

            _VERBOSE ? console.log("analisis_level_selected fuente: " + analisis_level_selected_fuente) : _VERBOSE
            _VERBOSE ? console.log("analisis_level_selected sumidero: " + analisis_level_selected_sumidero) : _VERBOSE

            var s_filters = _res_display_module_net.getFilters(_componente_fuente.getVarSelArray(), TIPO_FUENTE, analisis_level_selected_fuente);
            var t_filters = _res_display_module_net.getFilters(_componente_sumidero.getVarSelArray(), TIPO_SUMIDERO, analisis_level_selected_sumidero);


            // TODO: Desarrollar funcionalidad para cargar subgrupos de SPECIES mas subgrupos externos
            var isexternalsource = false
            s_filters.forEach(function (grupo) {
                if(grupo.isexternaldata){
                    isexternalsource = true
                }
            })
            var id_fuente = _iddatafuente ? _iddatafuente : null
            id_fuente = isexternalsource ? id_fuente : null
            _loadexternaldatafuente = isexternalsource ? isexternalsource : false

            
            var isexternaltarget = false
            t_filters.forEach(function (grupo) {
                if(grupo.isexternaldata){
                    isexternaltarget = true
                }
            })
            
            var id_destino = _iddatadestino ? _iddatadestino : null
            id_destino = isexternaltarget ? id_destino : null
            _loadexternaldatadestino = isexternaltarget ? isexternaltarget : false

            
            _VERBOSE ? console.log("_loadexternaldatafuente: " + _loadexternaldatafuente) : _VERBOSE
            _VERBOSE ? console.log("_iddatafuente: " + id_fuente) : _VERBOSE

            _VERBOSE ? console.log("_loadexternaldatadestino: " + _loadexternaldatadestino) : _VERBOSE
            _VERBOSE ? console.log("_iddatadestino: " + id_destino) : _VERBOSE


            _nivelTaxonomico = $("#select_niveltaxon").find(":selected").val();
            _VERBOSE ? console.log("_nivelTaxonomico: " + _nivelTaxonomico) : _VERBOSE

            
            var footprint_region = parseInt($("#footprint_region_select").val());
            _VERBOSE ? console.log("footprint_region: " + footprint_region) : _VERBOSE

            var data_source = $("#source_select").val();
            _VERBOSE ? console.log("data_source: " + data_source) : _VERBOSE

            var grid_res_val = $("#grid_resolution").val();
            _VERBOSE ? console.log("grid_resolution: " + grid_res_val) : _VERBOSE

            var fossil = $("#chkFosil").is(':checked');
            
            // var rango_fechas = $("#sliderFecha").slider("values");
            var rango_fechas = [$("#fechaini").val(), $("#fechafin").val()];

            _VERBOSE ? console.log(rango_fechas) : _VERBOSE

            
            if (rango_fechas[0] == "" ) {rango_fechas[0] = undefined;} 
            if (rango_fechas[1] == "" ) {rango_fechas[1] = undefined;} 
            var chkFecha = $("#chkFecha").is(':checked');

            _VERBOSE ? console.log("fossil: " + fossil) : _VERBOSE
            
            _VERBOSE ? console.log(rango_fechas) : _VERBOSE
            _VERBOSE ? console.log("chkFecha: " + chkFecha) : _VERBOSE            

            
            _res_display_module_net.createLinkNodes(s_filters, t_filters, min_occ, grid_res_val, footprint_region, rango_fechas, chkFecha, fossil, analisis_level_selected_fuente, analisis_level_selected_sumidero, data_source, _loadexternaldatafuente, _loadexternaldatadestino, id_fuente, id_destino, _nivelTaxonomico);

            $("#show_gen").css('visibility', 'visible');

            // TODO: enlazar
            var data_link = creaCamposLinkConfiguracion();            
            var openmodal = false;
            _utils_module.getLinkToken(data_link, _MODULO, _url_api, _url_comunidad, openmodal)


        });

        $("#net_link").click(function () {
            window.location.replace(_url_front + "/geoportal_v0.1.html");
        });

        $("#btn_tutorial").click(function () {
            window.open(_url_front + "/docs/tutorial.pdf");
        });



        $("#show_gen").click(function () {

            _VERBOSE ? console.log("show_gen") : _VERBOSE;

            var data_link = creaCamposLinkConfiguracion();            
            _utils_module.getLinkToken(data_link, _MODULO, _url_api, _url_comunidad)

        });

        $("#lb_enlace").click(function () {
            _VERBOSE ? console.log("lb_enlace") : _VERBOSE
            $(this).select();
        })

        $("#accept_link").click(function () {

            $("#modalRegenera").modal("hide");

        });
        
        $("#footprint_region_select").change(function (e) {

            _REGION_SELECTED = parseInt($("#footprint_region_select").val());
            _REGION_TEXT_SELECTED = $("#footprint_region_select option:selected").text();

        });



        _SCROLL_INDEX = 0;

        // $("#specie_next").click(function () {

        //     if(_SCROLL_INDEX >= _SCROLL_SECTIONS.length-1)
        //         return;

        //     _SCROLL_INDEX = _SCROLL_INDEX + 1;

        //     // console.log(_SCROLL_SECTIONS[_SCROLL_INDEX]) 
            
        //     $('html, body').animate({
        //         scrollTop: $("#"+_SCROLL_SECTIONS[_SCROLL_INDEX]).offset().top - 40
        //     }, 1000);

        // });


        // $("#specie_before").click(function () {

        //     if(_SCROLL_INDEX == 0)
        //         return;

        //     _SCROLL_INDEX = _SCROLL_INDEX - 1;

        //     // console.log(_SCROLL_SECTIONS[_SCROLL_INDEX]) 
            
        //     $('html, body').animate({
        //         scrollTop: $("#"+_SCROLL_SECTIONS[_SCROLL_INDEX]).offset().top - 40
        //     }, 1000);

        // });


        // document.getElementById("tbl_hist_comunidad").style.display = "none";
        // document.getElementById("map_panel").style.display = "none";
        // document.getElementById("hist_map_comunidad").style.display = "none";
        // document.getElementById("graph_map_comunidad").style.display = "none";


        // Revisa si el sistema tiene una liaga de regeneración de análisis
        // _genLinkURL();

        $( "#login" ).click(function( event ) {

            console.log("login");

            var data = {
                email:$("#email").val(),
                password:$("#password").val(),
                aviso:$("#aviso").is(":checked"),
                callback: "http://localhost:8080/auth/myCallback"
            }
            var actionUrl = _url_auth + "/auth/login"
            // var actionUrl = _url_api + "/login2"
            
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: data,
                success: function(resp){
                  
                  console.log(resp)
                  console.log(resp.sessionid)
                  console.log(resp.session.cookie.expires)

                  document.cookie = "sessionid=" + resp.sessionid + "; max-age=" + resp.session.cookie.originalMaxAge +"; path=/";

                  if(resp.status == 0){
                    
                    // window.location.href = _url_front + "/micuenta.html";  

                    $("#loginbtn").text("Mi cuenta")
                    $("#loginbtn").attr("data-target", "")
                    $("#loginbtn").attr("href", _url_front + "/micuenta.html")

                    $('#modalLogin').modal('toggle');

                  }
                  else{
                    console.log(resp.message)
                  }
                  

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    
                    $("#errorlb").empty();

                    _VERBOSE ? console.log(jqXHR.responseJSON) : _VERBOSE;
                    
                    if(jqXHR.responseJSON.status == 1){

                      jqXHR.responseJSON.message.forEach(function (error){

                        _VERBOSE ? console.log(error.message) : _VERBOSE;

                        $('#errorlb').append('<p style="color:red;">' + error.message + '</p>');

                      });

                    }

                }
            });

        });

        // configurando valor inicial - no esta detectando el bloque de regiones inicial cuando es snib
        var option_source = $("#source_select").val();
        console.log("option_source: " + option_source)
        var active_config = _utils_module.getActiveConfig();
        console.log(active_config)

        $('#footprint_region_select option[value=2]').attr("selected", "selected");    
        $('#footprint_region_select option[value=2]').prop("selected", true);    

        if(option_source == "snib"){
            $.each(active_config.regiones, function (i, item) {
                
                _VERBOSE ? console.log("entra snib") : _VERBOSE;

                if(item.label != 'México'){

                    _VERBOSE ? console.log("entra difernte mexico") : _VERBOSE;
                    _VERBOSE ? console.log("item.value: " + item.value) : _VERBOSE;

                    $('#footprint_region_select option[value='+item.value+']').attr("disabled", "disabled");    
                    $('#footprint_region_select option[value='+item.value+']').prop("disabled", true);    
                }
                else{
                    $('#footprint_region_select option[value='+item.value+']').removeAttr("disabled");    
                    $('#footprint_region_select option[value='+item.value+']').removeProp("disabled");    
                }
            })
        }
        else{

            _VERBOSE ? console.log("entra gbif") : _VERBOSE;

            $.each(active_config.regiones, function (i, item) {
                    $('#footprint_region_select option[value='+item.value+']').removeAttr("disabled");    
                    $('#footprint_region_select option[value='+item.value+']').removeProp("disabled");    
            })
        }  


        $("#source_select").change(function (e) {

            _VERBOSE ? console.log("Cambia fuente de datos") : _VERBOSE;

            option_source = $("#source_select").val();
            // console.log("option_source: " + option_source)

            var active_config = _utils_module.getActiveConfig();
            // console.log(active_config)

            $('#footprint_region_select option[value=1]').attr("selected", "selected");    
            $('#footprint_region_select option[value=1]').prop("selected", true);    

            if(option_source == "snib"){

                $.each(active_config.regiones, function (i, item) {

                    // console.log(item)

                    if(item.label != 'México'){

                        // console.log(item.value)

                        $('#footprint_region_select option[value='+item.value+']').attr("disabled", "disabled");    
                        $('#footprint_region_select option[value='+item.value+']').prop("disabled", true);    
                    }
                    else{

                        $('#footprint_region_select option[value='+item.value+']').removeAttr("disabled");    
                        $('#footprint_region_select option[value='+item.value+']').removeProp("disabled");    

                    }

                })
                
            }
            else{
                // option gbif, tiene todas las regiones activas

                $.each(active_config.regiones, function (i, item) {

                        // console.log(item)
                        $('#footprint_region_select option[value='+item.value+']').removeAttr("disabled");    
                        $('#footprint_region_select option[value='+item.value+']').removeProp("disabled");    

                })


            }            

        });



    }



    function obtieneIdsDatosTerceros(){

        console.log("obtieneIdsDatosTerceros")
        _COUNTER_PETICIONES_PRECARGA = 0;
        _niveles_total = []

        // get params from url
        var current_url =  window.location.href;
        console.log(current_url)

        var url = new URL(current_url);
        var id_fuente = url.searchParams.get("id_fuente");
        console.log("id_fuente: " + id_fuente)
        if(id_fuente != null){
            _COUNTER_PETICIONES_PRECARGA++;
        }

        var id_destino = url.searchParams.get("id_destino");
        console.log("id_destino: " + id_destino)
        if(id_destino != null){
            _COUNTER_PETICIONES_PRECARGA++;
        }

        console.log("_COUNTER_PETICIONES_PRECARGA: " + _COUNTER_PETICIONES_PRECARGA)

        
        
        if(id_fuente != null || id_destino != null){
            
            console.log("consulta datos precargados")

            if(_session_value == undefined || _session_value == ""){
                
                console.log("se requiere realizar login nuevamente")
                _module_toast.showToast_CenterCenter("Es necesario autenticarse y recargar la pantalla nuevamente para realizar la carga de sus colecciones de datos", "warning");

                return

            }
            else{

                if(id_fuente != null){

                    console.log("consulta fuente")

                    $.ajax({
                        url: _url_api + "/loaddata/getLoadedDataById",
                        type: 'post',
                        data: {
                            id_data: id_fuente
                        },
                        dataType: "json",
                        success: function (resp) {

                            _VERBOSE ? console.log(resp) : _VERBOSE
                            _iddatafuente = null;

                            _COUNTER_PETICIONES_PRECARGA = _COUNTER_PETICIONES_PRECARGA - 1

                            if(resp.status == 0){

                                _iddatafuente = resp.data[0].id
                                _nivelesFuente = resp.data[0].niveles
                                var nombre_datos_fuente = resp.data[0].nombre_datos

                                console.log("_iddatafuente: " + _iddatafuente)
                                console.log("_nivelesFuente: " + _nivelesFuente)

                                var item_fuente = [{
                                    close: true,
                                    isexternaldata: true,
                                    elements: [{
                                        label: "NA >> NA",
                                        level: -1,
                                        path: "",
                                        type: "0"
                                    }],
                                    title: nombre_datos_fuente,
                                    groupid: "1",
                                    type: "0",
                                    value: [{
                                        label: "NA >> NA",
                                        level: -1,
                                        path: "",
                                        type: "0"
                                    }],
                                }]

                                _componente_fuente.setVarSelArray(item_fuente);
                                var groups_s = item_fuente.slice();
                                _componente_fuente.addUIItem(groups_s);

                                _loadexternaldatafuente = true


                                _nivelesFuente.forEach(function(item){
                                    if(_niveles_total.indexOf(item) == -1){
                                        _niveles_total.push(item)
                                    }
                                })

                                if(_COUNTER_PETICIONES_PRECARGA == 0){
                                    despliegaModalPrecarga()    
                                }
                                


                            }


                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;

                        }
                    });



                }

                if(id_destino != null){

                    console.log("consulta destino")

                    $.ajax({
                        url: _url_api + "/loaddata/getLoadedDataById",
                        type: 'post',
                        data: {
                            id_data: id_destino
                        },
                        dataType: "json",
                        success: function (resp) {

                            _VERBOSE ? console.log(resp) : _VERBOSE
                            _iddatadestino = null

                            _COUNTER_PETICIONES_PRECARGA = _COUNTER_PETICIONES_PRECARGA - 1

                            if(resp.status == 0){
                                
                                _iddatadestino = resp.data[0].id
                                _nivelesDestino = resp.data[0].niveles
                                var nombre_datos_destino = resp.data[0].nombre_datos

                                console.log("_iddatadestino: " + _iddatadestino)
                                console.log("_nivelesDestino: " + _nivelesDestino)


                                var item_destino = [{
                                    close: true,
                                    isexternaldata: true,
                                    elements: [{
                                        label: "NA >> NA",
                                        level: -1,
                                        path: "",
                                        type: "0"
                                    }],
                                    title: nombre_datos_destino,
                                    groupid: "1",
                                    type: "0",
                                    value: [{
                                        label: "NA >> NA",
                                        level: -1,
                                        path: "",
                                        type: "0"
                                    }],
                                }]

                                _componente_sumidero.setVarSelArray(item_destino);
                                var groups_t = item_destino.slice();
                                _componente_sumidero.addUIItem(groups_t);

                            }

                            _loadexternaldatadestino = true

                            _nivelesDestino.forEach(function(item){
                                if(_niveles_total.indexOf(item) == -1){
                                    _niveles_total.push(item)
                                }
                            })

                            if(_COUNTER_PETICIONES_PRECARGA == 0){
                                despliegaModalPrecarga()    
                            }


                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;

                        }
                    });

                }


            }

            

            

        }
        else{
            console.log("sin datos precargados")            
            _loadexternaldata = false
            _iddatafuente = null
            _iddatadestino = null
        }


    }


    function despliegaModalPrecarga(){

        console.log("despliegaModalPrecarga")

        _niveles_total.forEach(function(item){
                    
            if(item == "species" || item == "genus" || item == "family"){
                $("#select_niveltaxon").append("<option value=\"" + item + "\">" + item + "</option>")        
            }
            
        });

        $("#modalNivelTaxon").modal('show');

    }



    /**
     * Construye el objeto json que guarda la configuración de un análisis de nicho ejecutado previamente.
     *
     * @function creaCamposLinkConfiguracion
     */
    function creaCamposLinkConfiguracion(){

        console.log("creaCamposLinkConfiguracion redes");

        var data_link = {};

        data_link.tipo = "comunidad"

        var subgroups_source = []
        var subgroups_target = []

        if(_loadexternaldatafuente || _loadexternaldatadestino){
            
            // _module_toast.showToast_BottomCenter(_iTrans.prop('lb_gen_values'), "warning");
            _module_toast.showToast_BottomCenter("La regeneración de este análisis requerirá tener el conjunto de datos que fue utilizado y estar autenticado en el momento de usar esta liga.", "warning");

            

            if(_loadexternaldatafuente){

                data_link.id_fuente = _iddatafuente
                subgroups_source = [{title:"na",type:"na",groupid:-1}];
                
            }
            else{
                subgroups_source = _componente_fuente.getVarSelArray();
            }

            data_link.loadexternaldatafuente = _loadexternaldatafuente

            if(_loadexternaldatadestino){

                data_link.id_destino = _iddatadestino
                subgroups_target = [{title:"na",type:"na",groupid:-1}];
                
            }
            else{
                subgroups_target = _componente_sumidero.getVarSelArray();
            }

            data_link.loadexternaldatadestino = _loadexternaldatadestino          

        }
        else{

            subgroups_source = _componente_fuente.getVarSelArray();
            subgroups_target = _componente_sumidero.getVarSelArray();

        }

        console.log(subgroups_source)
        console.log(subgroups_target)
        
        data_link.sfilters = subgroups_source;

        data_link.tfilters = subgroups_target;

        data_link.min_occ = parseInt($("#occ_number").val());

        data_link.grid_res = parseInt($("#grid_resolution").val());

        console.log("***** grid_res: " + data_link.grid_res)

        data_link.footprint_region = parseInt($("#footprint_region_select").val());

        data_link.num_sfilters = subgroups_source.length;

        data_link.num_tfilters = subgroups_target.length;

        data_link.fossil = $("#chkFosil").is(":checked");
        
        data_link.sfecha = $("#chkFecha").is(':checked');

        var rango_fechas = [$("#fechaini").val(), $("#fechafin").val()];

        if (rango_fechas[0] !== "" || rango_fechas[1] !== "" ) {
            data_link.lim_inf = rango_fechas[0];
            data_link.lim_sup = rango_fechas[1];
        } else {
            data_link.lim_inf = undefined;
            data_link.lim_sup = undefined;
        }

        console.log(data_link)

        return data_link;

    }
    
    
    
    // function _loadCountrySelect() {

    //     _VERBOSE ? console.log("_loadCountrySelect") : _VERBOSE

    //     $.ajax({
    //         url: _url_api + "/niche/especie/getAvailableCountriesFootprint",
    //         type: 'post',
    //         dataType: "json",
    //         success: function (resp) {

    //             var data = resp.data;
    //             _VERBOSE ? console.log(data) : _VERBOSE

    //             $.each(data, function (i, item) {

    //                 if (i === 0) {
    //                     $('#footprint_region_select').append('<option selected="selected" value="' + item.footprint_region + '">' + item.country + '</option>');
    //                 } else {
    //                     $('#footprint_region_select').append($('<option>', {
    //                         value: item.footprint_region,
    //                         text: item.country
    //                     }));
    //                 }

    //             });

    //         },
    //         error: function (jqXHR, textStatus, errorThrown) {
    //             _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;

    //         }
    //     });
    // }


    /**
     * Método para activar o desactivar de forma grupal la vista de loading o generación de resultados en los componentes de redes.
     *
     * @function loadingsNet
     * @public
     */
    function loadingsNet(a) {
      
      if (a == 1) {
        $('#charts').loading({ stoppable: true });
        $('#graph').loading({ stoppable: true });
        $('#map').loading({ stoppable: true });
        $('#tbl_container').loading({ stoppable: true });
      }
      else if (a == 0) {
        $('#charts').loading('stop');
        $('#graph').loading('stop');
        $('#map').loading('stop');
        $('#tbl_container').loading('stop');
      }
    }




    /**
     * Verifica si existe un token de regeneración de análisis, en caso de existir, obtiene y parsea el contenido 
     * en la URL para ejecutar el método encargado de obtener y procesar los parámetros guardados.
     *
     * @function _genLinkURL
     * 
     */
    function genLinkURL() {

        _VERBOSE ? console.log("genLinkURL redes") : _VERBOSE;

        if (_json_config == undefined) {
            return;
        }

        var token = _json_config.token;
        _getValuesFromToken(token);

    }


    /**
     * A partir de un token, obtiene los parámetros guardados de un análisis de redes y ejecuta el método que carga la información en la interfaz del sistema.
     *
     * @function _getValuesFromToken
     * 
     * @param {String} token - token relacionado a un conjunto de paramétros utilizados en un análisis.
     * 
     */
     function _getValuesFromToken(token) {

        _VERBOSE ? console.log("_getValuesFromToken") : _VERBOSE
        _VERBOSE ? console.log("token: " + token) : _VERBOSE

        $.ajax({
            url: _url_api + "/niche/especie/getValuesFromToken",
            type: 'post',
            data: {
                token: token,
                tipo: 'comunidad',
                isAuto: true
            },
            dataType: "json",
            success: function (resp) {

                _VERBOSE ? console.log(resp) : _VERBOSE

                if(resp.data[0].issaved == true){

                    console.log("Se reciben nodes y enlaces")

                    var params = JSON.parse(resp.data[0].parametros)
                    // _VERBOSE ? console.log(params) : _VERBOSE

                    var nodes = resp.data[0].nodes
                    var json_nodes = []
                    nodes.forEach(function (item) {json_nodes.push(JSON.parse(item));})
                    // _VERBOSE ? console.log(json_nodes) : _VERBOSE

                    var links = resp.data[0].links
                    var json_links = []
                    links.forEach(function (item) {json_links.push(JSON.parse(item));})
                    // _VERBOSE ? console.log(json_links) : _VERBOSE

                    _despliegaResultadoDesdeDatos(json_nodes, json_links, params);

                }
                else{

                    var all_data = resp.data[0].parametros;
                    _json_config = _utils_module.parseURL("?" + all_data);

                    var minOcc = _json_config.chkOcc ? parseInt(_json_config.chkOcc) : 5;

                    var gridRes = _json_config.gridRes ? parseInt(_json_config.gridRes) : 16;

                    var region = _json_config.region ? parseInt(_json_config.region) : 1;

                    var num_sfilters = parseInt(_json_config.num_sfilters);
                    
                    var num_tfilters = parseInt(_json_config.num_filters);

                    var chkFosil = _json_config.chkFosil ? _json_config.chkFosil === "true" : false;

                    var chkFec = _json_config.chkFec ? _json_config.chkFec === "true" : false;

                    var minFec = _json_config.minFec ? parseInt(_json_config.minFec) : undefined;

                    var maxFec = _json_config.maxFec ? parseInt(_json_config.maxFec) : undefined;

                    var rango_fechas = minFec != undefined && maxFec != undefined ? [minFec, maxFec] : undefined;
                    
                    var sfilters = [];

                    var filters = [];

                    for (i = 0; i < num_sfilters; i++) {
                        var item = _json_config["sfilters[" + i + "]"];
                        sfilters.push(JSON.parse(_json_config["sfilters[" + i + "]"]));
                    }

                    for (i = 0; i < num_tfilters; i++) {
                        var item = _json_config["tfilters[" + i + "]"];
                        filters.push(JSON.parse(_json_config["tfilters[" + i + "]"]));
                    }

                    _procesaValoresEnlace(sfilters, filters, minOcc, region, gridRes, chkFosil, chkFec, rango_fechas);

                }
                
                

                // _procesaValoresEnlace(sfilters, filters, chkVal, chkPrb, chkApr, chkFec, chkOcc, rango_fechas, chkFosil, gridRes, region, map_dPoints);

                $("#show_gen").css('visibility', 'hidden');

            },
            error: function (jqXHR, textStatus, errorThrown) {
                _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;

            }
        });


    }


    function _despliegaResultadoDesdeDatos(nodes, links, params){

        console.log("******** Se genera red con datos guardados")

        _res_display_module_net.cleanLegendGroups();
        _res_display_module_net.setNivelTaxonomico("species")
        _res_display_module_net.setNivelesFS("species", "species")

        configuraInterfazBase(params)

        _reubicaInterfaz(params, nodes)

        // se genera analisis de redes
        _res_display_module_net.createLinkNodesFromData(nodes, links)


    }

    function _reubicaInterfaz(params, nodes){

        console.log("******** Se reestructura interfaz")

        $("#section0").hide();
        $("#hist_map_comunidad").hide();
        $("#hist_comunidad, #tbl_container").appendTo("#main_container");

        var spname = ""
        if(params.source){
            spname = params.source
        }
        else{
            nodes.every(function (item) {
                console.log(item)
                if(item.grp == 1){
                    spname = item.generovalido + " " + item.especieepiteto
                    return false;
                }
            })
        }
        console.log("spname: " + spname)
        $("#tbl_comunidad").css('margin-top',50);
        $("#tbl_comunidad").prepend("<h3 style='margin-left:40px; font-style: oblique;'>" + spname + "</h3>")    

        

    }

    function configuraInterfazBase(params){

        console.log("configuraInterfazBase se configuran parametro desde enlace")

        $("#source_select").val(params.data_source);
        $("#footprint_region_select").val(parseInt(params.footprint_region));
        $("#grid_resolution").val(parseInt(params.grid_res));
        
        $("#occ_number").val(params.min_occ);
        $("#fechaini").val(params.lim_inf);
        $("#fechafin").val(params.lim_sup);

        if(params.data){
            $("#chkFecha").prop('checked', true);
        }
        else{
            $("#chkFecha").prop('checked', false);
        }
        if(params.fosil){
            $("#chkFosil").prop('checked', true);
        }
        else{
            $("#chkFosil").prop('checked', false);
        }        


    }
    
    
    /**
     * De los parámetros obtenidos de un token, configura y asigna los valores guardados en los componentes de la interfaz para regenerar nuevamente el análisis de redes.
     *
     * @function _procesaValoresEnlace
     * 
     * @param {json} subgroups_s - JSON  con el grupo de variables seleccionado para los nodos fuente
     * @param {json} subgroups_t - JSON  con el grupo de variables seleccionado para los nodos destino
     * @param {integer} nimOcc - Número mínimo de ocurrencias en nj para ser considerado en los cálculos
     * @param {integer} region - Identificador de la región donde se guardo el análisis
     * @param {integer} resolution - Resolución de la malla para ser considerado en los cálculos
     */
    function _procesaValoresEnlace(subgroups_s, subgroups_t, nimOcc, region, resolution, chkFosil, chkFec, rango_fechas) {

        _VERBOSE ? console.log("_procesaValoresEnlace") : _VERBOSE;

        var type_time_s = 0;
        var type_time_t = 0;
        var num_items = 0;

        if (chkFec) {
            $("#chkFecha").prop('checked', true);
            $("#lb_sfecha").text(_iTrans.prop('lb_si'));
        } else {
            $("#chkFecha").prop('checked', false);
            $("#lb_sfecha").text(_iTrans.prop('lb_no'));
        }

        if (chkFosil) {
            $("#chkFosil").prop('checked', true);
            $("#labelFosil").text(_iTrans.prop('lb_si'));
        } else {
            $("#chkFosil").prop('checked', false);
            $("#labelFosil").text(_iTrans.prop('lb_no'));
        }

        if (rango_fechas !== undefined) {
            $("#fechaini").val(rango_fechas[0]);
            $("#fechafin").val(rango_fechas[1]);
        }


        $("#occ_number").val(nimOcc);

        $('#grid_resolution option[value=' + resolution + ']').attr('selected', 'selected');

        $('#footprint_region_select option[value=' + region + ']').attr('selected', 'selected');

        console.log(subgroups_s)
        console.log(subgroups_t)

        _componente_fuente.setVarSelArray(subgroups_s);
        _componente_sumidero.setVarSelArray(subgroups_t);

        var groups_s = subgroups_s.slice();
        var groups_t = subgroups_t.slice();

        console.log(groups_s)
        console.log(groups_t)

        _componente_fuente.addUIItem(groups_s);
        _componente_sumidero.addUIItem(groups_t);

        console.log("nodes.length: " + nodes.length)
        console.log("links.length: " + nodes.length)


    }
    


    /**
     * Método setter para la variable que almacena la URL del servidor.
     *
     * @function setUrlApi
     * @public
     * 
     * @param {string} url_api - URL del servidor
     */
    function setUrlApi(url_api) {
        _url_api = url_api
    }

    /**
     * Método setter para la variable que almacena la URL del cliente.
     *
     * @function setUrlFront
     * @public
     * 
     * @param {string} url_front - URL del cliente
     */
    function setUrlFront(url_front) {
        _url_front = url_front
    }

    /**
     * Método setter para la variable que almacena la URL de redes ecológicas.
     *
     * @function setUrlComunidad
     * @public
     * 
     * @param {string} url_comunidad - URL del cliente en redes ecológicas.
     */
    function setUrlComunidad(url_comunidad) {
        _url_comunidad = url_comunidad
    }


    /**
     * Método setter para la variable que almacena la URL base del api de auth
     *
     * @function setUrlApiAuth
     * @public
     * 
     * @param {string} url_auth - URL del api de auth
     */
    function setUrlApiAuth(url_auth) {
        _url_auth = url_auth;
    }


    // retorna solamente un objeto con los miembros que son públicos.
    return {
        startModule: startModule,
        loadModules: loadModules,
        loadingsNet: loadingsNet,
        setUrlFront: setUrlFront,
        setUrlApi: setUrlApi,
        setUrlComunidad: setUrlComunidad,
        setUrlApiAuth: setUrlApiAuth
    };


})();


$(document).ready(function () {

    // _VERBOSE ? console.log(config.url_front) : _VERBOSE
    // _VERBOSE ? console.log(config.url_api) : _VERBOSE
    // _VERBOSE ? console.log(config.url_nicho) : _VERBOSE
    // _VERBOSE ? console.log(config.url_comunidad) : _VERBOSE

    module_net.setUrlFront(config.url_front);
    module_net.setUrlApi(config.url_api);
    module_net.setUrlApiAuth(config.url_auth);

    module_net.setUrlComunidad(config.url_comunidad);
    module_net.startModule(config.verbose, parseFloat(config.version).toFixed(1) );

});
