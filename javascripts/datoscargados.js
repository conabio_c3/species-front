
/**
 * Éste módulo es el encargado de controlar la funcionalidad de la página de inicio.
 *
 * @module module_index
 */
var module_index = (function() {
    
    var _VERBOSE = true;

    _VERBOSE ? console.log("*** loading datoscargados ... ***") : _VERBOSE;

    var _language_module_index;
    var _toastr = toastr;
    var _iTrans;
    var _tipo_modulo;
    var _link_val;
    var _url_front;
    var _url_api;
    var _url_nicho;
    var _url_comunidad;
    var _utils_module;
    var _url_auth;

    /**
     * Método encargado de asignar el direccionamiento del ambiente del sistema, 
     * tanto para el ambiente gráfico del frontend, como la versión que será utilizada del backend.
     *
     * @function  startModule
     *
     * @param {integer} tipo_modulo - Módulo inicial que sera configurado. 1. Nicho ecológico, 2. Redes de inferencia y 3. Inicio
     */
    function startModule(tipo_modulo) {
        
        _VERBOSE ? console.log("startModule datoscargados") : _VERBOSE;
        
        _url_front = config.url_front
        _url_api = config.url_api
        _url_auth = config.url_auth
        _url_nicho = config.url_nicho
        _url_comunidad = config.url_comunidad
        _VERBOSE = config.verbose
        _tipo_modulo = 6; 
        _loaddataon = false;
        _json_data = [];
        _reader = new FileReader();
        _userid = 0;

        _utils_module = utils_module();
        _utils_module.startUtilsModule();
        _tbl_datalist = false;


        var actionUrl = _url_auth + "/auth/isAuth"
        console.log("actionUrl: " + actionUrl)

        var session_value = _utils_module.getCookie("sessionid");
        // var session_user = _utils_module.getCookie("sessionuser");
        console.log(session_value)
        // console.log(session_user)


        $.ajax({
            type: "POST",
            url: actionUrl,
            data: {
                sessionid: session_value
            },
            success: function(resp){
              
                var session = resp.session[0]
                console.log(session)

                if(resp.status == 0 ){
                    var profile = session.sess.user
                    console.log(profile)

                    _userid = profile.userid
                    console.log("User id: " + _userid)

                    actionUrl = _url_api + "/loaddata/getProfileDataList"

                    $.ajax({
                        type: "POST",
                        url: actionUrl,
                        data: {
                            userid: _userid
                        },
                        success: function(resp){
                          
                            console.log(resp)

                            if(resp.status == 0 ){

                                console.log(resp.data)

                                var data_list = []

                                resp.data.forEach(function(d, index) {

                                    var item_list = [];

                                    item_list.push('<input  type="radio" id="' + d.id +'_ninguno" name="data_purpose_' + index + '" value="ninguno" checked="checked"><br/><label style="font-size: 14px !important;" for="html">Ninguno</label><br> <input  type="radio" id="' + d.id +'_redes_fuente" name="data_purpose_' + index + '" value="redes_fuente">  <label style="font-size: 14px !important;" for="html">Redes Fuente</label><br><input type="radio" id="' + d.id +'_redes_destino" name="data_purpose_' + index + '" value="redes_destino">  <label style="font-size: 14px !important;" for="css">Redes Destino</label><br><input type="radio" id="' + d.id +'_nicho_obj" name="data_purpose_' + index + '" value="nicho_obj">  <label style="font-size: 14px !important;" for="javascript">Nicho Objetivo</label><br><input type="radio" id="' + d.id +'_nicho_covars" name="data_purpose_' + index + '" value="nicho_covars"><label style="font-size: 14px !important;" for="javascript"> Nicho Covariables</label>')
                                    item_list.push(d.id)
                                    item_list.push(d.nombre_datos)
                                    item_list.push(d.fecha_carga)
                                    item_list.push(d.fecha_expiracion)
                                    // item_list.push(d.tipo_info)
                                    item_list.push("<input type=\"button\" id=\"renew_" + d.id + "\" class=\"renewbtn\" name=\"renovar\" value=\"Renovar\"> <input style=\"margin-top:5px;\" type=\"button\" class=\"deletebtn\" id=\"delete_" + d.id + "\" name=\"eliminar\" value=\"Eliminar\"> ")
                                    
                                    data_list.push(item_list)

                                })

                                console.log(data_list)

                                if (_tbl_datalist != false) {
                                    // $('#relation-list').dataTable().fnClearTable();
                                    $('#example').dataTable().fnDestroy();
                                }
                                _tbl_datalist = true;
                                
                                var table_datalist = $('#example').DataTable({
                                    "dom": 'Bfrtip',
                                    "info": true,
                                    // "bSort": true,
                                    "bLengthChange": false,
                                    "bPaginate": true, // Pagination True
                                    "processing": true, // Pagination True
                                    "iDisplayLength": 10,
                                    "searching": true,
                                    // "scrollY": "300px",
                                    "scrollCollapse": true,
                                    "paging": false,
                                    data: data_list,
                                    "aaSorting": [],
                                    language: {
                                        // "sEmptyTable": _iTrans.prop('sEmptyTable'), 
                                        // "info": _iTrans.prop('info'),
                                        "search":  "Buscar: ",
                                        // "zeroRecords": _iTrans.prop('zeroRecords'),
                                        // "infoEmpty": _iTrans.prop('infoEmpty'),
                                        // "infoFiltered": _iTrans.prop('infoFiltered')
                                    }
                                });

                                
                            }

                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            
                            _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;
                            console.log("error al obtener lista de datos")
                            
                        }
                    });


                }

            },
            error: function(jqXHR, textStatus, errorThrown) {

                _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;
                console.log("usuario no autenticado")
                window.location.href = _url_front + "/login.html";  

            }
        });


        $("#cclose").click(function(){

            console.log("cclose click")

            var actionUrl = _url_auth + "/auth/closeSesion"

            $.ajax({
                type: "POST",
                url: actionUrl,
                data: {
                    sessionid: session_value
                },
                success: function(resp){

                    console.log(resp)

                    if(resp.status == 0){

                        _utils_module.delete_cookie("sessionid")
                        window.location.href = _url_front + "/login.html";

                    }


                },
                error: function(jqXHR, textStatus, errorThrown) {
                    _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;
                    console.log("no se puede cerrar la sesión ")
                }
            });

        });


        


        $(document).on("click", ".deletebtn", function(){

            console.log("eliminar data loaded")

            var idbtn = $(this).attr('id')
            var dataid = idbtn.split("_")[1]

            console.log("dataid: " + dataid)

            var actionUrl = _url_api + "/loaddata/deleteLoadedData"

            $.ajax({
                type: "POST",
                url: actionUrl,
                data: {
                    id_data: dataid
                },
                success: function(resp){

                    console.log(resp)

                    if(resp.status == 0){

                        Swal.fire({
                          position: 'center',
                          icon: 'success',
                          title: 'Colección eliminada',
                          text: 'La colección ha sido eliminada correctamente',
                          showConfirmButton: true,
                          timer: 5000
                        }).then((result) => {
                  
                            // if (result.isConfirmed) {
                                window.location.href = _url_front + "/datoscargados.html";
                            // }

                        })

                        

                    }
                    else{

                        Swal.fire({
                            position: 'center',
                            icon: 'warning',
                            title: 'Error al eliminar datos',
                            text: resp.message,
                            showConfirmButton: true,
                            timer: 5000
                        })


                    }


                },
                error: function(jqXHR, textStatus, errorThrown) {
                    _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;
                    
                    console.log("error al eliminar datos")

                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        title: 'Error al eliminar datos',
                        text: textStatus,
                        showConfirmButton: true,
                        timer: 5000
                    })
                }
            });

        })



        $(document).on("click", ".renewbtn", function(){

            console.log("actualiza vigencia data loaded")

            var idbtn = $(this).attr('id')
            var dataid = idbtn.split("_")[1]

            console.log("dataid: " + dataid)

            var actionUrl = _url_api + "/loaddata/renewLoadedData"

            $.ajax({
                type: "POST",
                url: actionUrl,
                data: {
                    id_data: dataid
                },
                success: function(resp){

                    console.log(resp)

                    if(resp.status == 0){

                        Swal.fire({
                          position: 'center',
                          icon: 'success',
                          title: 'Vigencia renovada',
                          text: 'La colección ha sido renovada 1 mes correctamente',
                          showConfirmButton: true,
                          timer: 5000
                        }).then((result) => {
                  
                            // if (result.isConfirmed) {
                                window.location.href = _url_front + "/datoscargados.html";
                            // }

                        })

                        

                    }
                    else{

                        Swal.fire({
                            position: 'center',
                            icon: 'warning',
                            title: 'Error al renovar datos',
                            text: resp.message,
                            showConfirmButton: true,
                            timer: 5000
                        })


                    }


                },
                error: function(jqXHR, textStatus, errorThrown) {
                    _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;
                    
                    console.log("error al renovar datos")

                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        title: 'Error al renovar datos',
                        text: textStatus,
                        showConfirmButton: true,
                        timer: 5000
                    })
                }
            });

        })


        $("#carga_datos").change(function(e){

            console.log("carga_datos clicked")

            // if(_loaddataon){
            //     _VERBOSE ? console.log("clean table") : _VERBOSE
            //     $('#wrapper' + i + "_" + id).empty();
            // }

            _reader.readAsArrayBuffer(e.target.files[0]);
            
            _reader.onload = function(e) {

                _VERBOSE ? console.log("inicio carga archivo") : _VERBOSE
                // _loaddataon = true;

                var data = new Uint8Array(_reader.result);
                var wb = XLSX.read(data,{type:'array', cellDates: true});
                var ws = wb.Sheets[wb.SheetNames[0]];

                var array_data = XLSX.utils.sheet_to_json(ws, { 
                    header: ["occurrenceid", "decimallatitude", "decimallongitude", "eventdate", "kingdom", "phylum", "order", "class", "family", "genus", "species", "scientificname", "taxonrank" ], 
                    skipHeader: false });
                console.log(array_data);



                var BreakException = {};
                // var column_index = d3.map([]);
                _json_data = []
                // var format = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;
                // var format = /[`!@#$%^&*()_+\=\[\]{};'"\\|,<>\?~]/; // se aceptan espacios, guiones intermedios, puntos, dos puntos y diagonales
                var format = /[`!@#$%^&*()+\=\[\]{};':"\\|,.<>\/?~]/; // se aceptan solo espacios, guiones intermedios, bajos números y letras 
                var errors = [];

                try {

                    array_data.every(function(item, index) {

                        // console.log(columns)
                        // console.log(index)

                        // descarta encabezado
                        if(index != 0){

                            // console.log("length: " + Object.keys(item).length)

                            // verifica longitud de la fila
                            if(Object.keys(item).length == 13){

                                // if(format.test(item.id)){
                                //     errors.push({message: "Valor: " + item.id + " Identificador con caracteres inválidos, linea: " + (index+1)})
                                // }

                                if(format.test(item.occurrenceid)){
                                    errors.push({message: "Valor: " + item.occurrenceid + " Id externo con caracteres inválidos, linea: " + (index+1)})
                                }


                                item.decimallongitude = parseFloat(item.decimallongitude)
                                if(_utils_module.isNumeric(item.decimallongitude) == false){
                                    errors.push({message: "Valor: " + item.decimallongitude + " Longitud no es número válido, linea: " + (index+1)})
                                }

                                item.decimallatitude = parseFloat(item.decimallatitude)
                                if(_utils_module.isNumeric(item.decimallatitude) == false){
                                    errors.push({message: "Valor: " + item.decimallatitude + " Latitud no es número válido, linea: " + (index+1)})
                                }

                                if(item.eventdate != "" && item.eventdate.split("/").length == 3){
                                    var datearray = item.eventdate.split("/")

                                    const date = new Date(datearray[2], datearray[1], datearray[0])
                                    const result_date = date.toLocaleDateString("es-MX", { year: "numeric",month: "2-digit",day: "2-digit"})
                                    item.eventdate = moment(result_date, 'DD/MM/YYYY').format('YYYY-MM-DD')
                                    // console.log(item.eventdate)

                                    if(moment(item.eventdate, "YYYY-MM-DD", true).isValid() == false){
                                        errors.push({message: "Valor: " + item.eventdate + " Fecha no válida en el formato indicado (DD/MM/YYYY), linea: " + (index+1)})
                                    }

                                }
                                else{
                                    item.eventdate = "99/99/9999"
                                }
                                

                                
                                if(format.test(item.kingdom)){
                                    errors.push({message: "Valor: " + item.kingdom + " Reino con caracteres inválidos, linea: " + (index+1)})
                                }

                                if(format.test(item.phylum)){
                                    errors.push({message: "Valor: " + item.phylum + " Phylum con caracteres inválidos, linea: " + (index+1)})
                                }

                                if(format.test(item.class)){
                                    errors.push({message: "Valor: " + item.class + " Clase con caracteres inválidos, linea: " + (index+1)})
                                }

                                if(format.test(item.order)){
                                    errors.push({message: "Valor: " + item.order + " Orden con caracteres inválidos, linea: " + (index+1)})
                                }

                                if(format.test(item.family)){
                                    errors.push({message: "Valor: " + item.family + " Familia con caracteres inválidos, linea: " + (index+1)})
                                }

                                if(format.test(item.genus)){
                                    errors.push({message: "Valor: " + item.genus + " Genero con caracteres inválidos, linea: " + (index+1)})
                                }

                                if(format.test(item.species)){
                                    errors.push({message: "Valor: " + item.species + " Especie con caracteres inválidos, linea: " + (index+1)})
                                }

                                if(format.test(item.scientificname)){
                                    errors.push({message: "Valor: " + item.scientificname + " Nombre científico con caracteres inválidos, linea: " + (index+1)})
                                }


                                if(format.test(item.taxonrank)){
                                    errors.push({message: "Valor: " + item.taxonrank + " Nivel taxonómico con caracteres inválidos, linea: " + (index+1)})
                                }
                                // else if(item.taxonrank != "family" && item.taxonrank != "familia" && item.taxonrank != "genus" && item.taxonrank != "género" && item.taxonrank != "species" && item.taxonrank != "especie"){
                                //     errors.push({message: "Valor: " + item.taxonrank + " Nivel taxonómico no reconocido, linea: " + (index+1)})
                                // }

                                // if(format.test(item.commonname)){
                                //     errors.push({message: "Valor: " + item.commonname + " Nombre común con caracteres inválidos, linea: " + (index+1)})
                                // }

                                _json_data.push(item)  

                            }
                            else{

                                $("#lista_errores").empty()
                                $("#lista_errores").show()
                                $("#lb_review_error").show()

                                errors.push({message: "Archivo con diferentes columnas a las requeridas"})
                                errors.forEach(function(error, index) {
                                    $("#lista_errores").append("<li>" + error.message + "</li>")
                                })

                                return false

                            } 

                        }  

                        return true                      

                    });


                    if(errors.length > 0){

                        console.log("Hubo errores en el archivo recibido")
                        console.log(errors)

                        $("#lista_errores").empty()
                        $("#lista_errores").show()
                        $("#lb_review_error").show()

                        errors.forEach(function(error, index) {
                            $("#lista_errores").append("<li>" + error.message + "</li>")
                        })

                        return

                    }

                    // console.log(_json_data)
                    console.log("_json_data longitud: " + _json_data.length)

                    // TODO: validaciones del tamaño del archivo

                    $("#lb_review").show()
                    $("#envia_datos").show()
                    $("#lb_nombrecol").show()
                    $("#nombre_coleccion").show()

                } 
                catch (e) {
                  if (e !== BreakException) 
                    throw e;
                }                 

            }

        })


        $("#envia_datos").click(function(e){

            console.log("envia_datos clicked")
            // console.log(_json_data)

            var nombre_coleccion = $("#nombre_coleccion").val();
            console.log("nombre_coleccion: " + nombre_coleccion)
            console.log("userid: " + _userid)

            $("#cargaloading").show()
            $('#cargaloading').loading({ stoppable: true }); 

            $.ajax({
                url: _url_api + "/loaddata/loadOccDataGroup",
                dataType: "json",
                type: "post",
                data: {
                    "userid": _userid,
                    "json_data": _json_data,
                    "nombre_coleccion": nombre_coleccion
                },
                success: function (resp) {

                    console.log(resp)
                    $('#cargaloading').loading('stop');

                    if(resp.status == "ok"){
                        location.reload();
                    }
                    else{

                        $("#cargaloading").hide();
                        $('#cargaloading').loading('stop');
                        $("#error_cargaloading").show();

                    }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    
                    $("#cargaloading").hide();
                    $('#cargaloading').loading('stop');

                    $("#error_cargaloading").show()

                    _VERBOSE ? console.log(jqXHR) : _VERBOSE;
                    _VERBOSE ? console.log(errorThrown) : _VERBOSE;
                    _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;
                }

            });            

        })


        $("#btn_use_data").click(function (){

            console.log("btn_use_data")

            // recuperar datos seleccionados
            var selected = [];
            var redesfuente_counter = 0;
            var redesdestino_counter = 0;
            var nichoobj_counter = 0;
            var nichocovars_counter = 0;

            $('input:checked').each(function() {
                console.log($(this))

                if($(this).attr('id').indexOf("ninguno") == -1){
                    selected.push($(this).attr('id'));    
                }

                if($(this).attr('id').indexOf("redes_destino") != -1){
                    redesdestino_counter++
                }
                if($(this).attr('id').indexOf("redes_fuente") != -1){
                    redesfuente_counter++
                }

                if($(this).attr('id').indexOf("nicho_obj") != -1){
                    nichoobj_counter++
                }
                if($(this).attr('id').indexOf("nicho_covars") != -1){
                    nichocovars_counter++
                }
                
            });

            console.log(selected)
            console.log("redesdestino_counter: " + redesdestino_counter)
            console.log("redesfuente_counter: " + redesfuente_counter)
            console.log("nichoobj_counter: " + nichoobj_counter)
            console.log("nichocovars_counter: " + nichocovars_counter)

            var idredes_destino
            var idredes_fuente
            var idnicho_obj
            var idnicho_covars

            // validar datos seleccionados
            if(selected.length < 0){
                console.log("debes seleccionar al menos una colección")
            }

            if(redesdestino_counter == 1 || redesfuente_counter == 1){

                console.log("Se selecciona al menos una colección de redes")   

                selected.forEach(function(item) {
                    
                    if(item.indexOf("redes_destino") != -1){
                        idredes_destino = item.split("_")[0]
                        console.log("idredes_destino: " + idredes_destino)
                    }
                    else if(item.indexOf("redes_fuente") != -1){
                        idredes_fuente = item.split("_")[0]
                        console.log("idredes_fuente: " + idredes_fuente)
                    }

                });

                // confirmar selección
                $("#data_description").text("Los datos a utilizar para Redes de Inferencia son:")
                
                var message = ""
                var redeslink = ""

                if(redesfuente_counter == 1){
                    message = "Id Datos Fuente: " + idredes_fuente
                    redeslink = _url_comunidad + "?id_fuente="+idredes_fuente
                    
                }
                if(redesfuente_counter == 1 && redesdestino_counter == 1){
                    message += " y Id Datos Destino: " + idredes_destino
                    redeslink += "&id_destino="+idredes_destino
                }
                else if(redesdestino_counter == 1){
                    message = "Id Datos Destino: " + idredes_destino
                    redeslink = _url_comunidad + "?id_destino="+idredes_destino
                }

                $("#data_description_2").text(message)    

                $("#redirect_withdata").text("Ir a Redes de Inferencia")                
                $("#redirect_withdata").attr("href",redeslink)

                $('#modalredirect').modal('show')


            }
            else if(nichoobj_counter == 1 || nichocovars_counter == 1){

                console.log("Se selecciona al menos una colección de nicho")   

                selected.forEach(function(item) {
                    
                    if(item.indexOf("nicho_obj") != -1){
                        idnicho_obj = item.split("_")[0]
                        console.log("idnicho_obj: " + idnicho_obj)
                    }
                    else if(item.indexOf("nicho_covars") != -1){
                        idnicho_covars = item.split("_")[0]
                        console.log("idnicho_covars: " + idnicho_covars)
                    }

                });

                // confirmar selección
                $("#data_description").text("Los datos a utilizar para Análisis de Nicho son:")
                
                var message = ""
                var nicholink = ""

                if(nichoobj_counter == 1){
                    message = "Id Datos Grupo Objetivo: " + idnicho_obj
                    nicholink = _url_nicho + "?id_gpoobj="+idnicho_obj
                    
                }
                if(nichoobj_counter == 1 && nichocovars_counter == 1){
                    message += " y Id Grupo Covariables: " + idnicho_covars
                    nicholink += "&id_gpocovars="+idnicho_covars
                }
                else if(nichocovars_counter == 1){
                    message = "Id Grupo Covariables: " + idnicho_covars
                    nicholink = _url_nicho + "?id_gpocovars="+idnicho_covars
                }


                $("#data_description_2").text(message)    

                $("#redirect_withdata").text("Ir a Nicho Ecológico")                
                $("#redirect_withdata").attr("href",nicholink)

                $('#modalredirect').modal('show')


            }
            else{

                console.log("Debes seleccionar al menos una colección de redes (fuente-destino) o una colección de nicho (objetivo-covariables)")   

            }
            
        });


    }


    function deleteDataLoaded(){

        console.log("eliminar data loaded")

        console.log(id)


    }


    
    
            
    /**
     * Método de configuración e inicialización de componentes utilizados en la primera vista del sistema.
     *
     * @function _initializeComponents
     * @public
     */
    function _initializeComponents() {

        _VERBOSE ? console.log("_initializeComponents") : _VERBOSE;

    }




    /**
     * Este método inicializa el módulo de internacionalización del sistema 
     * y la configuración e inicialización de la página de aterrizaje del sistema.
     *
     * @function loadModules
     */
    function loadModules() {
        
        _VERBOSE ? console.log("loadModules datoscargados") : _VERBOSE;
        
    }

    // retorna solamente un objeto con los miembros que son públicos.
    return {
        startModule: startModule,
        loadModules: loadModules
    };

})();

var modulo = 6
module_index.startModule(modulo)
