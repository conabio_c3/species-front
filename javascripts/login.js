
/**
 * Éste módulo es el encargado de controlar la funcionalidad de la página de inicio.
 *
 * @module module_index
 */
var module_index = (function() {
    
    var _VERBOSE = true;

    _VERBOSE ? console.log("*** loading login... ***") : _VERBOSE;

    var _language_module_index;
    var _toastr = toastr;
    var _iTrans;
    var _tipo_modulo;
    var _link_val;
    var _url_front;
    var _url_api;
    var _url_nicho;
    var _url_comunidad;
    var _utils_module;

    /**
     * Método encargado de asignar el direccionamiento del ambiente del sistema, 
     * tanto para el ambiente gráfico del frontend, como la versión que será utilizada del backend.
     *
     * @function  startModule
     *
     * @param {integer} tipo_modulo - Módulo inicial que sera configurado. 1. Nicho ecológico, 2. Redes de inferencia y 3. Inicio
     */
    function startModule(tipo_modulo) {
        
        _VERBOSE ? console.log("startModule login") : _VERBOSE;
        
        _url_front = config.url_front
        _url_api = config.url_api
        _url_nicho = config.url_nicho
        _url_comunidad = config.url_comunidad
        _VERBOSE = config.verbose
        _tipo_modulo = 3; // login
        
        // Se cargan los archivos de idiomas y depsues son cargados los modulos subsecuentes
        _VERBOSE ? console.log(this) : _VERBOSE

        $( "#login" ).click(function( event ) {

            console.log("login");

            var data = {
                email:$("#email").val(),
                password:$("#password").val(),
                aviso:$("#aviso").is(":checked"),
                callback: "http://localhost:8080/auth/myCallback"
            }
            var actionUrl = _url_api + "/auth/login"
            // var actionUrl = _url_api + "/login2"
            
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: data,
                success: function(resp){
                  
                  console.log(resp)
                  console.log(resp.sessionid)
                  console.log(resp.session.cookie.expires)

                  document.cookie = "sessionid=" + resp.sessionid + "; max-age=" + resp.session.cookie.originalMaxAge +"; path=/";

                  if(resp.status == 0){
                    window.location.href = _url_front + "/micuenta.html";  
                  }
                  else{
                    console.log(resp.message)
                  }
                  

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    
                    $("#errorlb").empty();

                    _VERBOSE ? console.log(jqXHR.responseJSON) : _VERBOSE;
                    
                    if(jqXHR.responseJSON.status == 1){

                      jqXHR.responseJSON.message.forEach(function (error){

                        _VERBOSE ? console.log(error.message) : _VERBOSE;

                        $('#errorlb').append('<p style="color:red;">' + error.message + '</p>');

                      });

                    }

                }
            });

        });

    }
    
            
    /**
     * Método de configuración e inicialización de componentes utilizados en la primera vista del sistema.
     *
     * @function _initializeComponents
     * @public
     */
    function _initializeComponents() {

        _VERBOSE ? console.log("_initializeComponents") : _VERBOSE;

    }


    /**
     * Este método inicializa el módulo de internacionalización del sistema 
     * y la configuración e inicialización de la página de aterrizaje del sistema.
     *
     * @function loadModules
     */
    function loadModules() {
        
        _VERBOSE ? console.log("loadModules Login") : _VERBOSE;
        
    }

    // retorna solamente un objeto con los miembros que son públicos.
    return {
        startModule: startModule,
        loadModules: loadModules
    };

})();

var modulo = 3
module_index.startModule(modulo)
