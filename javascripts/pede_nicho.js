
/**
 * Éste módulo es el encargado de inicializar los controladores de los diferentes módulos que son utilizados 
 * en la configuración y ejecución de los análisis de nicho ecológico.
 *
 * @module module_pede_nicho
 */
var module_nicho = (function () {

    var _TEST = false;
    var MOD_NICHO = 0;
    var _VERBOSE = true;
    var _VERSION = 0.0;
    var _REGION_SELECTED;
    var _REGION_TEXT_SELECTED;
    var _MODULO = "nicho";

    // actualizar este arreglo si cambian los ids de las secciones
    var _SCROLL_SECTIONS = ["section0","section1","map","myScrollableBlockEpsilonDecil","histcontainer_row"];
    var _SCROLL_INDEX = 0;

    // var _loadeddata = false;

    var _tipo_modulo = MOD_NICHO;

    var _map_module_nicho,
        _variable_module_nicho,
        _res_display_module_nicho,
        _region_module_nicho,
        _table_module,
        _histogram_module_nicho,
        _language_module_nicho,
        _module_toast, _utils_module;

    var _componente_fuente;

    var _url_front, _url_api, _url_nicho, _url_auth;

    var _url_geoserver = "http://geoportal.conabio.gob.mx:80/geoserver/cnb/wms?",
            _workspace = "cnb";

    var _iTrans;
    
    const _ACGetEntList = new AbortController();
    const signal = _ACGetEntList.signal;
    
    var _getEntListInProcess = false;

    var groupSpSelection = [];

    var _columns_name = ["anio_colecta", "es_fosil", "latitud", "longitud"];

    var map_taxon = new Map()
    map_taxon.set("reino", "kingdom");
    map_taxon.set("kingdom", "kingdom");
    map_taxon.set("phylum", "phylum");
    map_taxon.set("clase", "class");
    map_taxon.set("class", "class");
    map_taxon.set("orden", "order");
    map_taxon.set("order", "order");
    map_taxon.set("familia", "family");
    map_taxon.set("family", "family");
    map_taxon.set("genero", "genus");
    map_taxon.set("género", "genus");
    map_taxon.set("genus", "genus");
    map_taxon.set("especie", "species");
    map_taxon.set("species", "species");

    var _taxones = [];

    var _datafile_loaded = [];

    var _session_value;
    var _COUNTER_PETICIONES_PRECARGA = 0
    var _niveles_total = []

    var _loadexternaldataobj = false
    var _loadexternaldatacovars = false


    var _iddataobj = null
    var _iddatacovars = null
    var _nivelesobj = []
    var _nivelescovars = []
    var _nivelTaxonomico = ""

    var _item_obj;
    var _item_covars;




    /**
     * Este método inicializa el módulo de internacionalización del sistema 
     * y enlaza el método de configuración e inicialización para el análisis de nicho.
     *
     * @function startModule
     * 
     * @param {string} tipo_modulo - Identificador del módulo 0 para nicho y 1 para comunidad
     * @param {string} verbose - Bandera para desplegar modo verbose
     * @param {string} version - Version del front para envio al back
     */
    function startModule(verbose, version) {

        _VERBOSE = verbose;
        _VERSION = version

        // console.log("*** version " + version)
        _VERBOSE ? console.log("startModule") : _VERBOSE;


        // carga de modulo de utilidades base para todos los modulos
        _utils_module = utils_module();
        _utils_module.startUtilsModule();

        // modulo para mensaje de error, necesario antes de la carga del resto de modulos
        _module_toast = toast_module(_VERBOSE);
        _module_toast.startToast();

        // verificación si existe seisón
        _session_value = _utils_module.getCookie("sessionid");
        console.log("session_value:" + _session_value)

        
        // veriricación si es un usaurio autenticado, si es autentcado carga datos en la interfaz
        verificaAuthUser()
        
        // obtiene datos si hay cargados por terceros
        obtieneIdsDatosTerceros(this);

    }



    function _getVersionCompatibility(version) {

        console.log("_getVersionCompatibility");
        console.log("version: " + version);

        $.ajax({
            url: _url_api + "/niche/especie/getVersionCompatibility",
            type: 'post',
            data: {
                version: version,
                tipo: 'nicho'
            },
            dataType: "json",
            success: function (resp) {

                console.log(resp);

                if(resp.code != 200){
                    console.log(resp.message);
                    _module_toast.showToast_RigthTop(_iTrans.prop('lb_cache'), "warning");
                    $("#cache_lb").css('visibility', 'visible');
                }


            },
            error: function (jqXHR, textStatus, errorThrown) {
                _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;

            }
        });


    }




    /**
     * Método ejecutado después de la configuración del módulo de internacionalización. Se encarga de
     * inicializar los controladores de mapas, tablas, histogramas y otros componentes principales.
     *
     * @function loadModules
     * @public
     * 
     */
    async function loadModules() {

        _VERBOSE ? console.log("loadModules") : _VERBOSE;

        _iTrans = _language_module_nicho.getI18();

        // confirma compatibildiad de versiones
        _getVersionCompatibility(_VERSION)
    

        _histogram_module_nicho = histogram_module(_VERBOSE);
        _histogram_module_nicho.setLanguageModule(_language_module_nicho);
        _histogram_module_nicho.startHistogramModule();
        

        _map_module_nicho = map_module(_url_geoserver, _workspace, _VERBOSE, _url_api);
        _map_module_nicho.startMap(_language_module_nicho, _tipo_modulo, _histogram_module_nicho, _utils_module, this);


        _table_module = table_module(_VERBOSE);
        _table_module.startTableModule();


        _language_module_nicho.setTableModule(_table_module)

        _res_display_module_nicho = res_display_module(_VERBOSE, _url_api);

        _map_module_nicho.setDisplayModule(_res_display_module_nicho);

        _histogram_module_nicho.setDisplayModule(_res_display_module_nicho);


        // un id es enviado para diferenciar el componente del grupo de variables en caso de que sea mas de uno (caso comunidad)
        _variable_module_nicho = variable_module(_VERBOSE, _url_api);
        _variable_module_nicho.startVar(0, _language_module_nicho, _tipo_modulo, _map_module_nicho, _utils_module);


        var ids_comp_variables = ['fuente', 'target'];

        _componente_fuente = _variable_module_nicho.createSelectorComponent("variables", ids_comp_variables[0], "lb_panel_variables", true, false, false, false, true, true);

        _componente_target = _variable_module_nicho.createSelectorComponent("var_target", ids_comp_variables[1], "", false, true, true, true, false, false);

        // enlazando los modulos que tienen interacción en los procesos
        _res_display_module_nicho.startResDisplay(_map_module_nicho, _histogram_module_nicho, _table_module, _language_module_nicho, ids_comp_variables);

        // se envian datos dcargados por terceros si existen


        if(_loadexternaldataobj){
            _res_display_module_nicho.set_iddataobj(_iddataobj)
            _res_display_module_nicho.set_loadexternaldataobj(_loadexternaldataobj)

            _componente_target.setVarSelArray(_item_obj);
            var groups_s = _item_obj.slice();
            _componente_target.addUIItem(groups_s);
        }

        if(_loadexternaldatacovars){

            _res_display_module_nicho.set_iddatacovars(_iddatacovars)
            _res_display_module_nicho.set_loadexternaldatacovars(_loadexternaldatacovars)

            _componente_fuente.setVarSelArray(_item_covars);
            var groups_t = _item_covars.slice();
            _componente_fuente.addUIItem(groups_t);
        }



        // se envia url con direccion a servidor zacatuche
        // _region_module_nicho = region_module(_url_api, _VERBOSE);
        // _region_module_nicho.startRegion(_map_module_nicho, _language_module_nicho);

        _language_module_nicho.addModuleForLanguage(_res_display_module_nicho, _histogram_module_nicho, _map_module_nicho, _variable_module_nicho);

            
        // Se manda a llamar ahora desde el módulo de mapa para nicho
        // _genLinkURL();        

        _initializeComponents();


    }


    
    //TODO: se desea encapsualr esta funcion para no repetir codigo en el resto de los archivos
    function verificaAuthUser(){

        console.log("verificaAuthUser")

        var actionUrl = _url_auth + "/auth/isAuth"
        // var actionUrl = _url_api + "/auth/isAuth"

        console.log("actionUrl: " + actionUrl)


        if(_session_value == undefined || _session_value == ""){
            console.log("sin sessionid")
            return
        }
        // console.log(_session_value)


        $.ajax({
            type: "POST",
            url: actionUrl,
            data: {
                sessionid: _session_value
            },
            success: function(resp){
              
                var session = resp.session[0]
                console.log(session)

                if(resp.status == 0 ){

                    var profile = session.sess.user
                    console.log(profile)

                    $("#loginbtn").text("Mi cuenta")
                    $("#loginbtn").attr("data-target", "")
                    $("#loginbtn").attr("href", _url_front + "/micuenta.html")

                    
                }
                else{

                    console.log("usuario no autenticado")
                    
                    _session_value = undefined
                    _utils_module.delete_cookie("sessionid")
                    
                    $("#loginbtn").text("Login")
                    $("#loginbtn").attr("data-target", "#modalLogin")
                    $("#loginbtn").attr("href", "")

                }

                return null;

            },
            error: function(jqXHR, textStatus, errorThrown) {
                _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;
                console.log("usuario no autenticado")
                
                _session_value = undefined
                _utils_module.delete_cookie("sessionid")
                
                $("#loginbtn").text("Login")
                $("#loginbtn").attr("data-target", "#modalLogin")
                $("#loginbtn").attr("href", "")

            }
        });

    }



    /**
     * Inicializa y configura los diferentes componetes secundarios que son necesarios para la ejecución de un análisis de niho.
     *
     * @function _initializeComponents
     */
    function _initializeComponents() {

        _VERBOSE ? console.log("_initializeComponents") : _VERBOSE;

        $("#lb_do_apriori").text(_iTrans.prop('lb_no'));
        $("#lb_mapa_prob").text(_iTrans.prop('lb_no'));
        $("#fechaini").val("");
        $("#fechafin").val("");
            

        function forceNumeric() {
            var $input = $(this);
            $input.val($input.val().replace(/[^\d]+/g, ''));
        }


        $('body').on('propertychange input', 'input[type="number"]', forceNumeric);


        // checkbox que se activa cuando se desea realizar el proceso de validación. (Proceso de validación todavia no implementado)
        $("#chkValidation").click(function (event) {

            console.log("cambia validacion");

            var $this = $(this);

            if ($this.is(':checked')) {

                $("#labelValidation").text("Si");

                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_validacion_act'), "info");

            } else {

                $("#labelValidation").text("No");
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_validacion_des'), "info");

            }

        });


        // checkbox que se activa cuando se desea tomar en cuanta un minimo de ocurrencias
        $("#chkMinOcc").click(function (event) {

            var $this = $(this);

            if ($this.is(':checked')) {

                $("#occ_number").prop("disabled", false);

                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_minocc_act'), "info");

            } else {

                $("#occ_number").prop("disabled", true);

                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_minocc_des'), "info");

            }

        });


        $("#chkFosil").click(function (event) {

            var $this = $(this);

            if ($this.is(':checked')) {

                $("#labelFosil").text("Si");

                _regenMessage();
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_fosil_act'), "info");

            } else {

                $("#labelFosil").text("No");

                _regenMessage();

                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_fosil_des'), "info");

            }

        });


        // checkbox que se activa cuando se desea tomar en cuanta un minimo de ocurrencias
        $("#chkFecha").click(function (event) {

            var $this = $(this);

            if ($this.is(':checked')) {
                
                // $("#sliderFecha").slider("enable");

                $("#lb_sfecha").text(_iTrans.prop('lb_si'));

                _regenMessage();
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_chkfecha'), "info");

            } else {

                $("#lb_sfecha").text(_iTrans.prop('lb_no'));

                _regenMessage();
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_chkfecha_des'), "info");

            }

        });


        $("#footprint_region_select").change(function (e) {

            _VERBOSE ? console.log("Cambiando a " + parseInt($("#footprint_region_select").val())) : _VERBOSE

            _REGION_SELECTED = parseInt($("#footprint_region_select").val());
            _REGION_TEXT_SELECTED = $("#footprint_region_select option:selected").text();
            _map_module_nicho.changeRegionView(_REGION_SELECTED);

            _regenMessage();

        });


        $("#grid_resolution").change(function (e) {

            _VERBOSE ? console.log("Cambia grid resolución") : _VERBOSE;
            // No es necesario regenerar resultados
            _regenMessage();

        });




        // configurando valor inicial - no esta detectando el bloque de regiones inicial cuando es snib
        var option_source = $("#source_select").val();
        console.log("option_source: " + option_source)
        var active_config = _utils_module.getActiveConfig();
        console.log(active_config)

        $('#footprint_region_select option[value=2]').attr("selected", "selected");    
        $('#footprint_region_select option[value=2]').prop("selected", true);    

        if(option_source == "snib"){
            $.each(active_config.regiones, function (i, item) {
                
                _VERBOSE ? console.log("entra snib") : _VERBOSE;

                if(item.label != 'México'){

                    _VERBOSE ? console.log("entra difernte mexico") : _VERBOSE;
                    _VERBOSE ? console.log("item.value: " + item.value) : _VERBOSE;

                    $('#footprint_region_select option[value='+item.value+']').attr("disabled", "disabled");    
                    $('#footprint_region_select option[value='+item.value+']').prop("disabled", true);    
                }
                else{
                    $('#footprint_region_select option[value='+item.value+']').removeAttr("disabled");    
                    $('#footprint_region_select option[value='+item.value+']').removeProp("disabled");    
                }
            })
        }
        else{

            _VERBOSE ? console.log("entra gbif") : _VERBOSE;

            $.each(active_config.regiones, function (i, item) {
                    $('#footprint_region_select option[value='+item.value+']').removeAttr("disabled");    
                    $('#footprint_region_select option[value='+item.value+']').removeProp("disabled");    
            })
        } 

        $("#source_select").change(function (e) {

            _VERBOSE ? console.log("Cambia fuente de datos") : _VERBOSE;

            option_source = $("#source_select").val();
            // console.log("option_source: " + option_source)

            var active_config = _utils_module.getActiveConfig();
            // console.log(active_config)

            $('#footprint_region_select option[value=1]').attr("selected", "selected");    
            $('#footprint_region_select option[value=1]').prop("selected", true);    

            if(option_source == "snib"){

                $.each(active_config.regiones, function (i, item) {

                    // console.log(item)

                    if(item.label != 'México'){

                        // console.log(item.value)

                        $('#footprint_region_select option[value='+item.value+']').attr("disabled", "disabled");    
                        $('#footprint_region_select option[value='+item.value+']').prop("disabled", true);    
                    }
                    else{

                        $('#footprint_region_select option[value='+item.value+']').removeAttr("disabled");    
                        $('#footprint_region_select option[value='+item.value+']').removeProp("disabled");    

                    }

                })
                
            }
            else{
                // option gbif, tiene todas las regiones activas

                $.each(active_config.regiones, function (i, item) {

                        // console.log(item)
                        $('#footprint_region_select option[value='+item.value+']').removeAttr("disabled");    
                        $('#footprint_region_select option[value='+item.value+']').removeProp("disabled");    

                })


            }            

        });


        // checkbox que se activa cuando se desea realizar el proceso de validación. (Proceso de validación todavia no implementado)
        $("#chkApriori").click(function (event) {

            var $this = $(this);

            if ($this.is(':checked')) {
                $("#lb_do_apriori").text(_iTrans.prop('lb_si'));
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_apriori_act'), "info");

            } else {
                $("#lb_do_apriori").text(_iTrans.prop('lb_no'));
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_apriori_desc'), "info");
            }

        });


        // checkbox que se activa cuando se desea realizar el proceso de validación. (Proceso de validación todavia no implementado)
        $("#chkMapaProb").click(function (event) {

            var $this = $(this);

            if ($this.is(':checked')) {
                $("#lb_mapa_prob").text(_iTrans.prop('lb_si'));
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_mapprob_act'), "info");

            } else {
                $("#lb_mapa_prob").text(_iTrans.prop('lb_no'));
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_status_mapprob_des'), "info");
            }

        });


        // deshabilita controles temporales
        $("#chkFecha").prop('disabled', false);


        // $("#sliderFecha").slider({
        //     disabled: false
        // });


        $("#nicho_link").click(function () {
            window.location.replace(_url_front + "/comunidad_v0.1.html");
        });

        $("#btn_tutorial").click(function () {
            window.open(_url_front + "/docs/tutorial.pdf");
        });

        _SCROLL_INDEX = 0;

        $("#specie_next").click(function () {

            if(_SCROLL_INDEX >= _SCROLL_SECTIONS.length-1)
                return;

            _SCROLL_INDEX = _SCROLL_INDEX + 1;

            // console.log(_SCROLL_SECTIONS[_SCROLL_INDEX]) 
            
            $('html, body').animate({
                scrollTop: $("#"+_SCROLL_SECTIONS[_SCROLL_INDEX]).offset().top - 40
            }, 1000);

            

        });


        $("#specie_before").click(function () {

            if(_SCROLL_INDEX == 0)
                return;

            _SCROLL_INDEX = _SCROLL_INDEX - 1;

            // console.log(_SCROLL_SECTIONS[_SCROLL_INDEX]) 
            
            $('html, body').animate({
                scrollTop: $("#"+_SCROLL_SECTIONS[_SCROLL_INDEX]).offset().top - 40
            }, 1000);

            

        });


        $('.ct-sliderPop-close').on('click', function () {
            $('.sliderPop').hide();
            $('.ct-sliderPop-container').removeClass('open');
            $('.sliderPop').removeClass('flexslider');
            $('.sliderPop .ct-sliderPop-container').removeClass('slides');
        });


        $("#reload_map").click(function () {

            _VERBOSE ? console.log("reload_map") : _VERBOSE;

            $("#sp_download").css('visibility', 'visible');
            $("#sp_download_csv").css('visibility', 'visible');

            // Hacer visble hasta que se ejecuta servicio de capa deciles
            $("#sp_download_decil").css('visibility', 'hidden');


            var region = _REGION_SELECTED;

            var groupDatasetTotal = _componente_target.getGroupDatasetTotal()

            _VERBOSE ? console.log(groupDatasetTotal) : _VERBOSE

            if(groupDatasetTotal.length == 0){
                console.log("No species selected");
                _module_toast.showToast_BottomCenter(_iTrans.prop('msg_noespecies_selected'), "warning");
                return;
            }


            if(groupDatasetTotal[0].isexternaldata){

                _loadexternaldataobj = true
                _taxones = [];

            }
            else{

                _loadexternaldataobj = false
                _taxones = [];
                _iddataobj = null

                $.each(groupDatasetTotal, function(index_i, grupo){

                    console.log(grupo);

                    $.each(grupo.elements, function(index_j, sp_grupo){
                        
                        var array_sp = sp_grupo.label.split(">>");

                        var temp_item = {};

                        temp_item["taxon_rank"] = map_taxon.get(array_sp[0].trim().toLowerCase());
                        temp_item["value"] = array_sp[1].trim();
                        temp_item["title"] = grupo.title;
                        temp_item["nivel"] = parseInt(sp_grupo.level); //0:root, 1:reino, etc...
                        _taxones.push(temp_item);
                    })

                })

            }

            _res_display_module_nicho.set_loadexternaldataobj(_loadexternaldataobj)
            _res_display_module_nicho.set_iddataobj(_iddataobj)

            console.log(_taxones);

            var val_process = $("#chkValidation").is(':checked');
            var data_source = $("#source_select").val();

            var grid_res = $("#grid_resolution").val();
            var footprint_region = parseInt($("#footprint_region_select").val());

            // _nivelTaxonomico = $("#select_niveltaxon").find(":selected").val();
            
            console.log("_iddataobj: " + _iddataobj);
            console.log("_loadexternaldataobj: " + _loadexternaldataobj);
            // _VERBOSE ? console.log("_nivelTaxonomico: " + _nivelTaxonomico) : _VERBOSE


            // _map_module_nicho.loadD3GridMX(val_process, grid_res, footprint_region, _taxones);

            _map_module_nicho.busca_especie_grupo(_taxones, footprint_region, val_process, grid_res, data_source, _iddataobj, _loadexternaldataobj);

            // se establece que el funcionamiento es por seleccion de especie
            // _loadeddata = false;
            // _loadexternaldataobj = false

        });


        $("#show_gen").click(function (e) {

            _VERBOSE ? console.log("show_gen") : _VERBOSE;

            var data_link = creaCamposLinkConfiguracion();            
            _utils_module.getLinkToken(data_link, _MODULO, _url_api, _url_nicho);
            // _getLinkToken(data_link);

        });


        $("#accept_link").click(function () {

            $("#modalRegenera").modal("hide");
            document.execCommand("copy");
            console.log('se copia url con toker');
        });



        $('#modalRegenera').on('shown.bs.modal', function (e) {

            $('#modalRegenera input[type="text"]')[0].select();

        });


        var _exist_tblload = false;

        $('#csv_load').change(function(e){

            console.log("selecciona archivo");

            _VERBOSE ? console.log("_exist_tblload: " + _exist_tblload) : _VERBOSE
            
            if(_exist_tblload){
                _VERBOSE ? console.log("clean table") : _VERBOSE
                // $('#tbl_spload').dataTable().fnDestroy();    
                $('#wrapper').empty();
            }


            var reader = new FileReader();
            reader.readAsArrayBuffer(e.target.files[0]);
            
            reader.onload = function(e) {

                _VERBOSE ? console.log("carga archivo") : _VERBOSE
                _exist_tblload = true;

                var data = new Uint8Array(reader.result);

                var wb = XLSX.read(data,{type:'array'});

                var ws = wb.Sheets[wb.SheetNames[0]];

                var json = XLSX.utils.sheet_to_json(ws, { header: 1, raw: true });

                // console.log(json);

                
                var BreakException = {};
                var column_index = d3.map([]);
                // var htmlstr = "";

                // htmlstr += "<table>"

                try {

                    json.forEach(function(columns) {

                        // htmlstr += "<th>"

                        // _VERBOSE ? console.log(columns) : _VERBOSE
                        columns.forEach(function (column, index) {


                            // _VERBOSE ? console.log(column) : _VERBOSE
                            if(_columns_name.indexOf(column) != -1){
                                
                                // htmlstr += "<td>" + column + "</td>"
                                column_index.set(index, column)

                            }
                        });

                        // htmlstr += "</th>"

                        throw BreakException;

                    });

                } 
                catch (e) {
                  if (e !== BreakException) throw e;
                }


                // _VERBOSE ? console.log(column_index.values()) : _VERBOSE

                if(column_index.values().length != 4){
                    _module_toast.showToast_BottomCenter("El excel no contiene las columnas requeridas, verifica el formato requerido", "error");
                    return;
                }

                _datafile_loaded = [];


                json.forEach(function(columns, indexi) {

                    // Descarta encabezados
                    if(indexi != 0){

                        // _VERBOSE ? console.log(columns) : _VERBOSE

                        // htmlstr += "<tr>"

                        var temp = {}

                        columns.forEach(function (column, indexj) {
                            
                            // _VERBOSE ? console.log(column + " " +indexj) : _VERBOSE

                            if(column_index.has(indexj)){

                                // _VERBOSE ? console.log(column_index.get(indexj)) : _VERBOSE

                                switch(column_index.get(indexj)){

                                    case _columns_name[0]:
                                        // column = column != "" && column != undefined ? column : ""
                                        temp.anio = column == "" ? 9999 : column; // Se agrega validación en caso de que datos no tengan fecha
                                        // htmlstr += "<td>" + column + "</td>"
                                        break;

                                    case _columns_name[1]:
                                        // column = column != "" && column != undefined ? column : ""
                                        temp.fosil = column == "" ? "NO" : column;
                                        // htmlstr += "<td>" + column + "</td>"
                                        break;

                                    case _columns_name[2]:
                                        // column = column != "" && column != undefined ? column : ""
                                        temp.latitud = column
                                        // htmlstr += "<td>" + column + "</td>"
                                        break;

                                    case _columns_name[3]:
                                        // column = column != "" && column != undefined ? column : ""
                                        temp.longitud = column
                                        // htmlstr += "<td>" + column + "</td>"
                                        break;

                                }

                            }  
                    
                        });

                        // htmlstr += "</tr>"

                        _datafile_loaded.push(temp)                     

                    }
                    
                });

                // console.log(_datafile_loaded);

                // htmlstr += "</table>"

                // var html = XLSX.utils.sheet_to_html(ws, { header: 1, raw: true });
                // console.log(html);

                var htmlstr = XLSX.write(wb,{type:'binary',bookType:'html'});
                // _VERBOSE ? console.log(htmlstr) : _VERBOSE


                $('#wrapper')[0].innerHTML += htmlstr;
                // _VERBOSE ? console.log($("#wrapper table")) : _VERBOSE

                var raw_table = $("#wrapper table");
                // console.log(raw_table);
                raw_table.attr("id","tbl_spload");
                raw_table.prop("id","tbl_spload")                    

                // $('#tbl_spload').DataTable({
                //     language: {
                //         "sEmptyTable": _iTrans.prop('sEmptyTable'), 
                //         "info": _iTrans.prop('info'),
                //         "search": _iTrans.prop('search') + " ",
                //         "zeroRecords": _iTrans.prop('zeroRecords'),
                //         "infoEmpty": _iTrans.prop('infoEmpty'),
                //         "infoFiltered": _iTrans.prop('infoFiltered')
                //     }
                // });

                
            }
        });


        

        // Funcionalidad de mensaje de bienvenida
        $('#nomostrar_msg').change(function() {
            
            if(this.checked) {
                // _VERBOSE ? console.log("agrega cookie") : _VERBOSE
                Cookies.set('cacher_modal', true)
            }
            else{
                // _VERBOSE ? console.log("borra cookie") : _VERBOSE
                Cookies.remove('cacher_modal')
            }
            
        });

        $("#cancel_wel").click(function(){
            $("#modalInicio").modal("hide");
        })

        $("#tuto_wel").click(function(){

            $("#modalInicio").modal("hide");
            _language_module_nicho.liveTutorialConfNiche();
            
        })

        $("#usecase_wel").click(function(){
            $("#modalInicio").modal("hide");
            $("#modalDemo").modal("show")
        })


        // se ejecutan los modulos necesarios para iniciar el proceso de obteción de epsilon y score y visualización de tablas, histogramas y mapa
        $("#get_esc_ep").click(function () {

            _VERBOSE ? console.log("get_esc_ep") : _VERBOSE;

            // if (_taxones.length === 0 && _loadeddata == false) {
            if (_taxones.length === 0 && _loadexternaldataobj == false) {
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_error_especie'), "warning");
                return;
            } 

            startNicheAnalisis();

            // Guarda ultima configuración ejecutada
            var data_link = creaCamposLinkConfiguracion();
            var openmodal = false;
            _utils_module.getLinkToken(data_link, _MODULO, _url_api, _url_nicho, openmodal);
            // _getLinkToken(data_link, openmodal);


        });



        $( "#login" ).click(function( event ) {

            console.log("login");

            var data = {
                email:$("#email").val(),
                password:$("#password").val(),
                aviso:$("#aviso").is(":checked"),
                callback: "http://localhost:8080/auth/myCallback"
            }
            
            var actionUrl = _url_auth + "/auth/login"
            // var actionUrl = _url_api + "/auth/login"
            
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: data,
                success: function(resp){
                  
                  console.log(resp)
                  console.log(resp.sessionid)
                  console.log(resp.session.cookie.expires)

                  document.cookie = "sessionid=" + resp.sessionid + "; max-age=" + resp.session.cookie.originalMaxAge +"; path=/";

                  if(resp.status == 0){
                    
                    // window.location.href = _url_front + "/micuenta.html";  

                    $("#loginbtn").text("Mi cuenta")
                    $("#loginbtn").attr("data-target", "")
                    $("#loginbtn").attr("href", _url_front + "/datoscargados.html")

                    $('#modalLogin').modal('toggle');

                  }
                  else{
                    console.log(resp.message)
                  }
                  

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    
                    $("#errorlb").empty();

                    _VERBOSE ? console.log(jqXHR.responseJSON) : _VERBOSE;
                    
                    if(jqXHR.responseJSON.status == 1){

                      jqXHR.responseJSON.message.forEach(function (error){

                        _VERBOSE ? console.log(error.message) : _VERBOSE;

                        $('#errorlb').append('<p style="color:red;">' + error.message + '</p>');

                      });

                    }

                }
            });

        });


        // Despliega un popup inicial para mostrar al usuario un tutorial
        _showTutorialPopUp();


    }



    function obtieneIdsDatosTerceros(this_pede){

        console.log("obtieneIdsDatosTerceros")

        _COUNTER_PETICIONES_PRECARGA = 0;
        _niveles_total = []

        // get params from url
        var current_url =  window.location.href;
        console.log(current_url)

        var url = new URL(current_url);
        var id_gpoobj = url.searchParams.get("id_gpoobj");
        console.log("id_gpoobj: " + id_gpoobj)
        
        if(id_gpoobj != null){
            _COUNTER_PETICIONES_PRECARGA++;
        }

        var id_gpocovars = url.searchParams.get("id_gpocovars");
        console.log("id_gpocovars: " + id_gpocovars)
        if(id_gpocovars != null){
            _COUNTER_PETICIONES_PRECARGA++;
        }

        console.log("_COUNTER_PETICIONES_PRECARGA: " + _COUNTER_PETICIONES_PRECARGA)


        // Si no hay datos de terceros por cargar, iniciliza lenguaje y depsues carga el resto de modulos
        if(_COUNTER_PETICIONES_PRECARGA == 0){
            // Se cargan los archivos de idiomas y depsues son cargados los modulos subsecuentes
            _language_module_nicho = language_module(_VERBOSE, _VERSION);

            // Despues de cargar el idioma, se manda a llamar a la funcion de loadmodules
            _language_module_nicho.startLanguageModule(this_pede, _tipo_modulo);
        }

        
        
        if(id_gpoobj != null || id_gpocovars != null){
            
            console.log("consulta datos precargados")


            // Manda mensjae de alerta si el usuario no esta autenticado
            if(_session_value == undefined || _session_value == ""){
                
                console.log("se requiere realizar login nuevamente")
                _module_toast.showToast_BottomCenter("Es necesario autenticarse y recargar la pantalla nuevamente para usar sus colecciones de datos", "warning");
                // return
            }

            
            // se obtienen los datos cargados por tercero aun que no este autenticado
            if(id_gpoobj != null){

                console.log("consulta dato objetivo")

                $.ajax({
                    url: _url_api + "/loaddata/getLoadedDataById",
                    type: 'post',
                    data: {
                        id_data: id_gpoobj
                    },
                    dataType: "json",
                    success: function (resp) {

                        _VERBOSE ? console.log(resp) : _VERBOSE
                        _iddataobj = null;
                        _item_obj = []

                        _COUNTER_PETICIONES_PRECARGA = _COUNTER_PETICIONES_PRECARGA - 1

                        if(resp.status == 0){

                            _iddataobj = resp.data[0].id
                            // _res_display_module_nicho.set_iddataobj(_iddataobj)

                            _nivelesobj = resp.data[0].niveles
                            var nombre_datos_fuente = resp.data[0].nombre_datos

                            console.log("_iddataobj: " + _iddataobj)
                            console.log("_nivelesobj: " + _nivelesobj)

                            _item_obj = [{
                                close: true,
                                isexternaldata: true,
                                elements: [{
                                    label: "NA >> NA",
                                    level: -1,
                                    path: "",
                                    type: "0"
                                }],
                                title: nombre_datos_fuente,
                                groupid: "1",
                                type: "0",
                                value: [{
                                    label: "NA >> NA",
                                    level: -1,
                                    path: "",
                                    type: "0"
                                }],
                            }]

                            _loadexternaldataobj = true
                            // _VERBOSE ? console.log("_loadexternaldataobj: " + _loadexternaldataobj) : _VERBOSE;

                            // _nivelesobj.forEach(function(item){
                            //     if(_niveles_total.indexOf(item) == -1){
                            //         _niveles_total.push(item)
                            //     }
                            // })


                            if(_COUNTER_PETICIONES_PRECARGA == 0){
                                
                                despliegaModalPrecarga()   

                                // Se cargan los archivos de idiomas y depsues son cargados los modulos subsecuentes
                                _language_module_nicho = language_module(_VERBOSE, _VERSION);

                                // Despues de cargar el idioma, se manda a llamar a la funcion de loadmodules
                                _language_module_nicho.startLanguageModule(this_pede, _tipo_modulo); 
                            }
                            
                        }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;

                    }
                });

            }

            if(id_gpocovars != null){

                console.log("consulta covars")

                $.ajax({
                    url: _url_api + "/loaddata/getLoadedDataById",
                    type: 'post',
                    data: {
                        id_data: id_gpocovars
                    },
                    dataType: "json",
                    success: function (resp) {

                        _VERBOSE ? console.log(resp) : _VERBOSE
                        _iddatacovars = null
                        _item_covars = []

                        _COUNTER_PETICIONES_PRECARGA = _COUNTER_PETICIONES_PRECARGA - 1

                        if(resp.status == 0){
                            
                            _iddatacovars = resp.data[0].id
                            // _res_display_module_nicho.set_iddatacovars(_iddatacovars)

                            _nivelescovars = resp.data[0].niveles
                            var nombre_datos_destino = resp.data[0].nombre_datos

                            console.log("_iddatacovars: " + _iddatacovars)
                            console.log("_nivelescovars: " + _nivelescovars)


                            _item_covars = [{
                                close: true,
                                isexternaldata: true,
                                elements: [{
                                    label: "NA >> NA",
                                    level: -1,
                                    path: "",
                                    type: "0"
                                }],
                                title: nombre_datos_destino,
                                groupid: "1",
                                type: "0",
                                value: [{
                                    label: "NA >> NA",
                                    level: -1,
                                    path: "",
                                    type: "0"
                                }],
                            }]


                        }

                        _loadexternaldatacovars = true
                        // _VERBOSE ? console.log("_loadexternaldatacovars: " + _loadexternaldatacovars) : _VERBOSE;

                        // _nivelescovars.forEach(function(item){
                        //     if(_niveles_total.indexOf(item) == -1){
                        //         _niveles_total.push(item)
                        //     }
                        // })

                        if(_COUNTER_PETICIONES_PRECARGA == 0){
                            despliegaModalPrecarga() 

                            // Se cargan los archivos de idiomas y depsues son cargados los modulos subsecuentes
                            _language_module_nicho = language_module(_VERBOSE, _VERSION);

                            // Despues de cargar el idioma, se manda a llamar a la funcion de loadmodules
                            _language_module_nicho.startLanguageModule(this_pede, _tipo_modulo);    
                        }


                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;

                    }
                });

            }


            

        }
        else{
            console.log("sin datos precargados")            
            _loadexternaldatacovars = false
            _loadexternaldataobj = false
            _iddatacovars = null
            _iddataobj = null
        }


    }


    function consultaDatosCargadosPorID(){


    }



    function despliegaModalPrecarga(){

        console.log("despliegaModalPrecarga")

        // _niveles_total.forEach(function(item){
            // if(item == "species" || item == "genus" || item == "family"){
            //     $("#select_niveltaxon").append("<option value=\"" + item + "\">" + item + "</option>")        
            // }
        // });

        $("#modalNivelTaxon").modal('show');

    }


    /**
     * Se encarga de validar si el modal de primera visita el sistema es desplegado al usuario.
     *
     * @function _showTutorialPopUp
     * @private
     */
    function _showTutorialPopUp(){

        if (Cookies.get('cacher_modal')) {
            $("#modalInicio").remove();
        } else {
            $("#modalInicio").modal("show");
        }        

    }


    /**
     * Cambia la tonalidad del color del botón que obtiene las ocurrencias 
     * de la especie para saber si es necesario volver a ejecutarlo.
     * Actualmente esta en desuso.
     *
     * @function _regenMessage
     * @private
     */
    function _regenMessage() {

        if ($("#reload_map").hasClass("btn-primary") && _map_module_nicho.get_specieTarget()) {

            _module_toast.showToast_BottomCenter(_iTrans.prop('lb_gen_values'), "warning");
            $("#reload_map").addClass('btn-success').removeClass('btn-primary');
            

        }

    }


    /**
     * Construye el objeto json que guarda la configuración de un análisis de nicho ejecutado previamente.
     *
     * @function creaCamposLinkConfiguracion
     */
    function creaCamposLinkConfiguracion(){

        console.log("creaCamposLinkConfiguracion nicho");

        var data_link = {};
        var subgroups_target = []

        if(_loadexternaldataobj || _loadexternaldatacovars){
            
            // _module_toast.showToast_BottomCenter(_iTrans.prop('lb_gen_values'), "warning");
            _module_toast.showToast_BottomCenter("La regeneración de este análisis requerirá tener el conjunto de datos que fue utilizado y estar autenticado en el momento de usar esta liga.", "warning");

            subgroups_target = [{title:"na",type:"na",groupid:-1}];

            if(_loadexternaldataobj){

                data_link.iddataobj = _iddataobj
            }
            if(_loadexternaldatacovars){

                data_link.iddatacovars = _iddatacovars
            }

            data_link.loadexternaldataobj = _loadexternaldataobj
            data_link.loadexternaldatacovars = _loadexternaldatacovars


        }
        else{
            
            subgroups_target = _componente_target.getVarSelArray();

        }
        
        var subgroups = _componente_fuente.getVarSelArray();    
        data_link.tfilters = subgroups;            

        data_link.tipo = "nicho"
        
        // data_link.taxones = _taxones;
        data_link.sfilters = subgroups_target;

        data_link.val_process = $("#chkValidation").is(":checked");
        data_link.idtabla = data_link.val_process === true ? _res_display_module_nicho.getValidationTable() : "no_table";
        data_link.mapa_prob = $("#chkMapaProb").is(":checked");
        data_link.fossil = $("#chkFosil").is(":checked");
        data_link.apriori = $("#chkApriori").is(':checked');
        data_link.sfecha = $("#chkFecha").is(':checked');

        
        // var rango_fechas = $("#sliderFecha").slider("values");
        var rango_fechas = [$("#fechaini").val(), $("#fechafin").val()];

        // if (rango_fechas[0] !== $("#sliderFecha").slider("option", "min") || rango_fechas[1] !== $("#sliderFecha").slider("option", "max")) {
        //     data_link.lim_inf = rango_fechas[0];
        //     data_link.lim_sup = rango_fechas[1];
        // } else {
        //     data_link.lim_inf = undefined;
        //     data_link.lim_sup = undefined;
        // }

        if (rango_fechas[0] !== "" || rango_fechas[1] !== "" ) {
            data_link.lim_inf = rango_fechas[0];
            data_link.lim_sup = rango_fechas[1];
        } else {
            data_link.lim_inf = undefined;
            data_link.lim_sup = undefined;
        }
        

        data_link.min_occ = $("#chkMinOcc").is(':checked') === true ? parseInt($("#occ_number").val()) : 0;
        data_link.grid_res = $("#grid_resolution").val();
        data_link.data_source = $("#source_select").val();
        data_link.footprint_region = parseInt($("#footprint_region_select").val());

        data_link.discardedFilterids = _map_module_nicho.get_discardedPoints().values().map(function (value) {
            return value.feature.properties.gridid
        });
        // console.log(data_link.discardedFilterids);

        
        console.log(data_link);

        return data_link;

    }

    
    /**
     * Genera un token de recuperación de un análisis previamente ejecutado.
     *
     * @function _getLinkToken
     * 
     * @param {String} data_link - Cadena que contiene los parametros selecicoandos por el usuario en el análisis.
     * @param {Boolean} openmodal - Bandera por default en true, para desplegar el modal donde se encuentra el token generado.
     * 
     */
    // function _getLinkToken(data_link, openmodal = true) {

    //     console.log("_getLinkToken");
    //     _VERBOSE ? console.log(data_link) : _VERBOSE

    //     $.ajax({
    //         url: _url_api + "/niche/especie/getToken",
    //         type: 'post',
    //         data: data_link,
    //         dataType: "json",
    //         success: function (resp) {

    //             var cadena_ini = _url_nicho + '#link/?';
    //             var tokenlink = resp.data[0].token;

    //             console.log("token: " + tokenlink);

    //             if(openmodal){
    //                 $("#modalRegenera").modal();
    //                 $("#lb_enlace").val(cadena_ini + "token=" + tokenlink);
    //             }
    //             else{
    //                 _VERBOSE ? console.log("guarda token en input hidden") : _VERBOSE
                    
    //                 // Asigna a input hidden para reporteo
    //                 $("#link_gen").val(cadena_ini + "token=" + tokenlink)
    //                 link_description = "________________________________________________________________________________\nNo borrar la siguiente liga:\n" + $("#link_gen").val();
    //                 window.ATL_JQ_PAGE_PROPS.fieldValues.description = "Describe a continuación el problema encontrado o escribe tus comentarios:\n\n\n\n\n\n\n\n\n" + link_description
    //                 _VERBOSE ? console.log(window.ATL_JQ_PAGE_PROPS.fieldValues.description) : _VERBOSE
    //                 _VERBOSE ? console.log("link_gen: " + $("#link_gen").val()) : _VERBOSE

    //             }

    //         },
    //         error: function (jqXHR, textStatus, errorThrown) {
    //             _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;

    //         }
    //     });

    // }


    /**
     * A partir de un token, obtiene los parámetros guardados de un análisis de nicho y ejecuta el método que carga la información en la interfaz del sistema.
     *
     * @function _getValuesFromToken
     * @param {String} token - token relacionado a un conjunto de paramétros utilizados en un análisis.
     * 
     */
    function _getValuesFromToken(token) {

        console.log("_getValuesFromToken");
        console.log("token: " + token);

        $.ajax({
            url: _url_api + "/niche/especie/getValuesFromToken",
            type: 'post',
            data: {
                token: token,
                tipo: 'nicho',
                isAuto: true
            },
            dataType: "json",
            success: function (resp) {

                console.log(resp);

                if(resp.data.length === 0){
                    _module_toast.showToast_BottomCenter(_iTrans.prop('lb_error_link'), "error");
                    return
                }

                if(resp.data[0].issaved == true){

                    console.log("Se recibe respuesta de analisis de nicho")

                    var params = JSON.parse(resp.data[0].parametros)
                    _VERBOSE ? console.log(params) : _VERBOSE

                    var analisis_nicho_result = JSON.parse(resp.data[0].nicho)
                    _VERBOSE ? console.log(analisis_nicho_result) : _VERBOSE
                    
                    var data = analisis_nicho_result.data
                    // _VERBOSE ? console.log(data) : _VERBOSE

                    var data_freq = analisis_nicho_result.data_freq
                    // _VERBOSE ? console.log(data_freq) : _VERBOSE

                    var data_score_cell = analisis_nicho_result.data_score_cell
                    // _VERBOSE ? console.log(data_score_cell) : _VERBOSE

                    var data_freq_cell = analisis_nicho_result.data_freq_cell
                    // _VERBOSE ? console.log(data_freq_cell) : _VERBOSE

                    var validation_data = analisis_nicho_result.validation_data
                    // _VERBOSE ? console.log(validation_data) : _VERBOSE

                    var percentage_avg = analisis_nicho_result.percentage_avg
                    // _VERBOSE ? console.log(percentage_avg) : _VERBOSE

                    var decil_cells = analisis_nicho_result.decil_cells
                    // _VERBOSE ? console.log(decil_cells) : _VERBOSE                    

                    _despliegaResultadoDesdeDatos(data, data_freq, data_score_cell, data_freq_cell, validation_data, percentage_avg, decil_cells, params);

                }
                else{

                    var all_data = resp.data[0].parametros;
                    _json_config = _utils_module.parseURL("?" + all_data);

                    var chkVal = _json_config.chkVal ? _json_config.chkVal === "true" : false;

                    var chkPrb = _json_config.chkPrb ? _json_config.chkPrb === "true" : false;

                    var chkFosil = _json_config.chkFosil ? _json_config.chkFosil === "true" : false;

                    var chkApr = _json_config.chkApr ? _json_config.chkApr === "true" : false;

                    var chkFec = _json_config.chkFec ? _json_config.chkFec === "true" : false;

                    var chkOcc = _json_config.chkOcc ? parseInt(_json_config.chkOcc) : undefined;

                    var minFec = _json_config.minFec ? parseInt(_json_config.minFec) : undefined;

                    var maxFec = _json_config.maxFec ? parseInt(_json_config.maxFec) : undefined;

                    var gridRes = _json_config.gridRes ? _json_config.gridRes : 16;

                    var region = _json_config.region ? parseInt(_json_config.region) : 1;

                    var data_source = _json_config.data_source ? _json_config.data_source : 'snib';

                    var rango_fechas = minFec != undefined && maxFec != undefined ? [minFec, maxFec] : undefined;

                    // recover deleted items
                    var num_dpoints = parseInt(_json_config.num_dpoints);
                    var map_dPoints = d3.map([]);
                    for (i = 0; i < num_dpoints; i++) {
                        var item = JSON.parse(_json_config["deleteditem[" + i + "]"]);
                        map_dPoints.set(item.feature.properties.gridid, item);
                    }
                    _VERBOSE ? console.log(map_dPoints.values()) : _VERBOSE

                    var num_sfilters = parseInt(_json_config.num_sfilters);
                    var sfilters = [];
                    // var loadeddata = false;
                    
                    // TODO: ****** Revisar funcionamento!!! No siempre las dos colecciones seran cargadas ****
                    // TODO: probar generacion y caga de link de regenreacion!!

                    
                    if(num_sfilters >= 1){
                        var temp = JSON.parse(_json_config["sfilters[0]"]);
                        _VERBOSE ? console.log(temp) : _VERBOSE
                        
                        if(temp["title"] == "na" && temp["groupid"] == -1){
                            // loadeddata = true;

                            // TODO: guardar ids de grupos en la generación de análisis
                            // loadexternaldatacovars = true
                            // loadexternaldataobj = true

                            // _module_toast.showToast_BottomCenter(_iTrans.prop('lb_error_link'), "error");
                            // _module_toast.showToast_BottomCenter("Este analisis necesita el conjunto de datos con el que fue generado", "warning");
                            _module_toast.showToast_BottomCenter("Este análisis necesita ser enlazado con una colección de datos de tus grupos de datos", "warning");
                        }
                        else{

                            for (i = 0; i < num_sfilters; i++) {
                                var item = _json_config["sfilters[" + i + "]"];
                                sfilters.push(JSON.parse(_json_config["sfilters[" + i + "]"]));
                            }

                        }
                    }

                    var num_filters = parseInt(_json_config.num_filters);
                    var filters = [];
                    for (i = 0; i < num_filters; i++) {
                        var item = _json_config["tfilters[" + i + "]"];
                        filters.push(JSON.parse(_json_config["tfilters[" + i + "]"]));
                    }

                    // se establece que el analisis fue por carga de datos
                    // _loadeddata = loadeddata
                    // _loadexternaldatacovars = loadexternaldatacovars
                    // _loadexternaldataobj = loadexternaldataobj
                    // _VERBOSE ? console.log("_loadeddata: " + _loadeddata) : _VERBOSE



                    _VERBOSE ? console.log("_session_value: " + _session_value) : _VERBOSE
                    // Revisar si en este momento las variables ya fueron cargadas 
                    _VERBOSE ? console.log("Desde link _loadexternaldatacovars: " + _loadexternaldatacovars) : _VERBOSE
                    _VERBOSE ? console.log("Desde link _loadexternaldataobj: " + _loadexternaldataobj) : _VERBOSE

                    // TODO: Si la sesion es invalida y existen datos cargados, mandar mensaje q no es posible generar el analisis hasta existir un login


                    _procesaValoresEnlace(sfilters, filters, chkVal, chkPrb, chkApr, chkFec, chkOcc, rango_fechas, chkFosil, gridRes, region, data_source, map_dPoints);

                    
                    $("#show_gen").css('visibility', 'hidden');


                }

                



            },
            error: function (jqXHR, textStatus, errorThrown) {
                _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;

            }
        });


    }

    function _despliegaResultadoDesdeDatos(data, data_freq, data_score_cell, data_freq_cell, validation_data, percentage_avg, decil_cells, params){

        console.log("_despliegaResultadoDesdeDatos")

        configuraInterfazBase(params)

        _reubicaInterfaz(params)

        _map_module_nicho.loadD3GridMX(false, params.grid_res, params.footprint_region, params.data_source, [], 0, false, true, data_score_cell);   

        _res_display_module_nicho.createNichoAnalysisFromData(data, data_freq, data_score_cell, data_freq_cell, validation_data, percentage_avg, decil_cells, params)


    }

    function _reubicaInterfaz(params){

        console.log("******** Se reestructura interfaz")

        $("#section0").hide();
        $("#section1").hide();
        $("#section3").hide();
        $("#lb_resultados").hide();
        
        var spname = ""
        if(params.target_group){

            var region = "";
            console.log(config)

            config.plataforma[0].regiones.forEach(function(item){
                if(params.footprint_region == item.value){
                    region = item.label
                }
            })

            spname = "<h3 style='margin-left:40px; font-style: oblique;'>" + params.target_group[0].value + "</h3>"
            spname += "<h4 style='margin-left:40px'> Región: " +  region + " | Resolución: " + params.grid_res + "km"  + "</h4>"
            // spname += "<h3 style='margin-left:40px'> Resolución: " + + "</h3>"
        }
        console.log("spname: " + spname)
        
        $("#section2").css('margin-top',50);
        $("#section2").prepend(spname);

        _map_module_nicho.changeRegionView(params.footprint_region)

    }

    function configuraInterfazBase(params){

        console.log("configuraInterfazBase se configuran parametro desde enlace")

        $("#source_select").val(params.data_source);
        $("#footprint_region_select").val(parseInt(params.footprint_region));
        $("#grid_resolution").val(parseInt(params.grid_res));
        
        $("#occ_number").val(params.min_occ);
        $("#fechaini").val(params.lim_inf);
        $("#fechafin").val(params.lim_sup);

        if(params.data){
            $("#chkFecha").prop('checked', true);
        }
        else{
            $("#chkFecha").prop('checked', false);
        }
        if(params.fosil){
            $("#chkFosil").prop('checked', true);
        }
        else{
            $("#chkFosil").prop('checked', false);
        }        


    }


    /**
     * Verifica si existe un token de regeneración de análisis, en caso de existir, obtiene y parsea el contenido 
     * en la URL para ejecutar el método encargado de obtener y procesar los parámetros guardados.
     *
     * @function _genLinkURL
     * @public
     * 
     */
    function genLinkURL() {

        _VERBOSE ? console.log("genLinkURL nicho") : _VERBOSE;

        if (_json_config == undefined) {
            return;
        }

        var token = _json_config.token;
        _getValuesFromToken(token);

    }

   
    /**
     * De los parámetros obtenidos de un token, configura y asigna los valores guardados en los componentes de la interfaz para regenerar nuevamente el análisis de nicho.
     *
     * @function _procesaValoresEnlace
     *
     * @param {json} subgroups_target - JSON  con el grupo de variables objetivo seleccionado 
     * @param {json} subgroups - JSON  con el grupo de variables seleccionado
     * @param {boleano} chkVal - Bandera si esta activado el proceso de validación
     * @param {boleano} chkPrb - Bandera si esta activado el mapa de probabilidad
     * @param {boleano} chkApr - Bandera si esta activado el cálculo con a priori
     * @param {boleano} chkFec - Bandera si esta activado el cálculo con registros sin fecha
     * @param {integer} chkOcc - Número mínimo de ocurrencias en nj para ser considerado en los cálculos
     * @param {array} rango_fechas - Rango de fecha para realizar los cálculos
     * @param {boleano} chkFosil - Bandera si esta activado la consideración de registros fósiles
     * @param {integer} gridRes - Resolución de la malla para ser considerado en los cálculos
     * @param {integer} region - Identificador de la región donde se guardo el análisis
     * @param {array} map_dPoints - Arreglo con los identificadores de celda que no deben ser considerados en el análisis
     */
    function _procesaValoresEnlace(subgroups_target, subgroups, chkVal, chkPrb, chkApr, chkFec, chkOcc, rango_fechas, chkFosil, gridRes, region, data_source, map_dPoints) {

        _VERBOSE ? console.log("_procesaValoresEnlace") : _VERBOSE;

        console.log("Region : " + region);
        var idreg = ["Estados"]; // Módulo por desarrollar
        
        if (chkFec) {
            $("#chkFecha").prop('checked', true);
            $("#lb_sfecha").text(_iTrans.prop('lb_si'));
        } else {
            $("#chkFecha").prop('checked', false);
            $("#lb_sfecha").text(_iTrans.prop('lb_no'));
        }

        if (chkVal) {
            $("#chkValidation").prop('checked', true);
            $("#labelValidation").text(_iTrans.prop('lb_si'));
        } else {
            $("#chkValidation").prop('checked', false);
            $("#labelValidation").text(_iTrans.prop('lb_no'));
        }

        if (chkPrb) {
            $("#chkMapaProb").prop('checked', true);
            $("#lb_mapa_prob").text(_iTrans.prop('lb_si'));
        } else {
            $("#chkMapaProb").prop('checked', false);
            $("#lb_mapa_prob").text(_iTrans.prop('lb_no'));
        }


        if (chkFosil) {
            $("#chkFosil").prop('checked', true);
            $("#labelFosil").text(_iTrans.prop('lb_si'));
        } else {
            $("#chkFosil").prop('checked', false);
            $("#labelFosil").text(_iTrans.prop('lb_no'));
        }

        if (chkApr) {
            $("#chkApriori").prop('checked', true);
            $("#lb_do_apriori").text(_iTrans.prop('lb_si'));
        } else {
            $("#chkApriori").prop('checked', false);
            $("#lb_do_apriori").text(_iTrans.prop('lb_no'));
        }


        if (chkOcc) {
            $("#chkMinOcc").prop('checked', true);
            $("#occ_number").prop("disabled", false);
            $("#occ_number").val(chkOcc);

        } else {
            $("#chkMinOcc").prop('checked', false);
            $("#occ_number").prop("disabled", true);
            $("#occ_number").val(chkOcc);
        }

        console.log(rango_fechas)

        $("#lb_range_result").text("");

        if (rango_fechas !== undefined) {

            $("#lb_range_result").text("("+ rango_fechas[0] + "-" + rango_fechas[1] +")");
            // $("#fechaini").val(rango_fechas[0]);
            // $("#fechafin").val(rango_fechas[1]);

        }

        console.log("gridRes: " + gridRes);

        // $("#grid_resolution option:selected").removeAttr("selected");
        $('#grid_resolution option[value=' + gridRes + ']').attr('selected', 'selected');

        console.log("grid_resolution: " + $('#grid_resolution').val());

        $('#footprint_region_select option[value=' + region + ']').attr('selected', 'selected');

        $('#source_select option[value=' + data_source + ']').attr('selected', 'selected');


        _VERBOSE ? console.log(subgroups_target) : _VERBOSE
        _VERBOSE ? console.log(subgroups) : _VERBOSE

        
        _taxones = [];


        if(!_loadexternaldataobj){

            _VERBOSE ? console.log("Se cargan valores de especie objetivo") : _VERBOSE

            _componente_target.setVarSelArray(subgroups_target);

            $.each(subgroups_target, function(index_i, grupo){

                console.log(grupo);

                $.each(grupo.value, function(index_j, sp_grupo){

                    var array_sp = sp_grupo.label.split(">>");

                    var temp_item = {};

                    temp_item["taxon_rank"] = map_taxon.get(array_sp[0].trim().toLowerCase());
                    temp_item["value"] = array_sp[1].trim();
                    temp_item["title"] = grupo.title;
                    temp_item["nivel"] = parseInt(sp_grupo.level); //0:root, 1:reino, etc...
                    _taxones.push(temp_item);

                })

            })

            _VERBOSE ? console.log(_taxones) : _VERBOSE

            _componente_target.addUIItem(subgroups_target.slice());

            _map_module_nicho.loadD3GridMX(chkVal, gridRes, region, data_source, _taxones);   

        }
        else{

            _VERBOSE ? console.log("Se envia id de datos cargados por terceros") : _VERBOSE

            _map_module_nicho.loadD3GridMX(chkVal, gridRes, region, data_source, _taxones, _iddataobj, _loadexternaldataobj);   

        }

        if(!_loadexternaldatacovars){

            _componente_fuente.addUIItem(subgroups.slice());
        
            _res_display_module_nicho.set_idReg(idreg);
      
            _componente_fuente.setVarSelArray(subgroups);
            
            _res_display_module_nicho.set_subGroups(subgroups);


            if (subgroups.length == 0) {
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_error_variable'), "warning");
            } 
            else {
                _module_toast.showToast_BottomCenter(_iTrans.prop('lb_gen_link'), "info");
            }

        }


        
        
        

    }

   
    /**
     * Agrupa todos los valores configurados por el usuario para iniciar el proceso de análisis de nicho.
     *
     * @function startNicheAnalisis
     * 
     */
    function startNicheAnalisis(){

        _VERBOSE ? console.log("startNicheAnalisis") : _VERBOSE;

        var num_items = 0, spid, idreg, subgroups, sp_target;

        
        $("#show_gen").css('visibility', 'visible');   
        $("#btn_tuto_steps_result").css('visibility', 'visible');
        
        // agregar validación para estados
        // idreg = _region_module_nicho.getRegionSelected();
        // _res_display_module_nicho.set_idReg(idreg);

        subgroups = _componente_fuente.getVarSelArray();

        _res_display_module_nicho.set_taxones(_taxones);
        
        var type_time = _componente_fuente.getTimeBioclim();

        _res_display_module_nicho.set_subGroups(subgroups);

        _res_display_module_nicho.set_typeBioclim(type_time);


        if (subgroups.length <= 0) {

            $("#show_gen").css('visibility', 'hidden');
            $("#btn_tuto_steps_result").css('visibility', 'hidden');
            $("#tuto_res").css('visibility', 'hidden');
            // $("#params_next").css('visibility', 'hidden');

            _module_toast.showToast_BottomCenter(_iTrans.prop('lb_error_variable'), "error");
            return;
        }


        _VERBOSE ? console.log(_map_module_nicho.get_discardedPoints()) : _VERBOSE

        _res_display_module_nicho.set_discardedPoints(_map_module_nicho.get_discardedPoints());
        _res_display_module_nicho.set_discardedCellFilter(_map_module_nicho.get_discardedCellFilter());
        _res_display_module_nicho.set_allowedCells(_map_module_nicho.get_allowedCells());


        var val_process = $("#chkValidation").is(':checked');
        var min_occ = $("#chkMinOcc").is(':checked');
        var mapa_prob = $("#chkMapaProb").is(':checked');
        var grid_res = $("#grid_resolution").val();
        var footprint_region = parseInt($("#footprint_region_select").val());
        var occ_target = _map_module_nicho.get_occTarget();
        var data_source = $("#source_select").val();

        console.log("grid_res: " + grid_res);
        console.log("occ_target: " + occ_target);

        var fossil = $("#chkFosil").is(':checked');


        
        // var rango_fechas = $("#sliderFecha").slider("values");
        var rango_fechas = [$("#fechaini").val(), $("#fechafin").val()];

        // if (rango_fechas[0] == $("#sliderFecha").slider("option", "min") && rango_fechas[1] == $("#sliderFecha").slider("option", "max")) {
        //     rango_fechas = undefined;
        // }

        // if (rango_fechas[0] == "" && rango_fechas[1] == "" ) {
        //      rango_fechas = undefined;   
        // }

        if (rango_fechas[0] == "" ) {
            rango_fechas[0] = undefined;
        } 

        if (rango_fechas[1] == "" ) {
            rango_fechas[1] = undefined;
        } 


        var chkFecha = $("#chkFecha").is(':checked');

        var slider_value = val_process ? true : false;

        var analisis_level_selected = _componente_fuente.analisis_level_selected
        _VERBOSE ? console.log("analisis_level_selected: " + analisis_level_selected) : _VERBOSE


        _VERBOSE ? console.log("_iddataobj: " + _iddataobj) : _VERBOSE
        _VERBOSE ? console.log("_loadexternaldataobj: " + _loadexternaldataobj) : _VERBOSE
        _VERBOSE ? console.log("_iddatacovars: " + _iddatacovars) : _VERBOSE
        _VERBOSE ? console.log("_loadexternaldatacovars: " + _loadexternaldatacovars) : _VERBOSE
        // _VERBOSE ? console.log("_nivelTaxonomico: " + _nivelTaxonomico) : _VERBOSE
        

        // Falta agregar la condición makesense. 
        // Cuando se realiza una consulta por region seleccioanda se verica que la especie objetivo se encuentre dentro de esta area
        _res_display_module_nicho.refreshData(num_items, val_process, slider_value, min_occ, mapa_prob, rango_fechas, chkFecha, fossil, grid_res, footprint_region, _datafile_loaded, analisis_level_selected, occ_target, data_source, _loadexternaldataobj, _loadexternaldatacovars, _iddataobj, _iddatacovars);


    }
    


    /**
     * Método setter para la variable que almacena la URL de la versión del API utilizada
     *
     * @function setUrlApi
     * @public
     * 
     * @param {string} url_api - URL del servidor
     */
    function setUrlApi(url_api) {
        _url_api = url_api
    }

    /**
     * Método setter para la variable que almacena la URL base del frontend
     *
     * @function setUrlFront
     * @public
     * 
     * @param {string} url_front - URL del cliente
     */
    function setUrlFront(url_front) {
        _url_front = url_front
    }

    /**
     * Método setter para la variable que almacena la URL base del frontend para el nicho ecológico
     *
     * @function setUrlNicho
     * @public
     * 
     * @param {string} url_nicho - URL del cliente en nicho ecológico
     */
    function setUrlNicho(url_nicho) {
        _url_nicho = url_nicho;
    }


    /**
     * Método setter para la variable que almacena la URL base del api de auth
     *
     * @function setUrlApiAuth
     * @public
     * 
     * @param {string} url_auth - URL del api de auth
     */
    function setUrlApiAuth(url_auth) {
        _url_auth = url_auth;
    }


    // retorna solamente un objeto con los miembros que son públicos.
    return {
        startModule: startModule,
        loadModules: loadModules,
        setUrlApi: setUrlApi,
        setUrlFront: setUrlFront,
        setUrlNicho: setUrlNicho,
        genLinkURL: genLinkURL,
        setUrlApiAuth: setUrlApiAuth
    };


})();


$(document).ready(function () {

    // _VERBOSE ? console.log(config.url_front) : _VERBOSE
    // _VERBOSE ? console.log(config.url_api) : _VERBOSE
    // _VERBOSE ? console.log(config.url_nicho) : _VERBOSE
    // _VERBOSE ? console.log(config.url_comunidad) : _VERBOSE

    module_nicho.setUrlFront(config.url_front);
    module_nicho.setUrlApi(config.url_api);
    module_nicho.setUrlApiAuth(config.url_auth);

    module_nicho.setUrlNicho(config.url_nicho);
    module_nicho.startModule(config.verbose, parseFloat(config.version).toFixed(1) );

});



