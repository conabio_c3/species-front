
/**
 * Éste módulo es el encargado de controlar la funcionalidad de la página de inicio.
 *
 * @module module_index
 */
var module_index = (function() {
    
    var _VERBOSE = true;

    _VERBOSE ? console.log("*** loading index... ***") : _VERBOSE;

    var _language_module_index;
    var _toastr = toastr;
    var _iTrans;
    var _tipo_modulo;
    var _link_val;

    var _url_front;
    var _url_api;
    var _url_nicho;
    var _url_comunidad;



    /**
     * Método encargado de asignar el direccionamiento del ambiente del sistema, 
     * tanto para el ambiente gráfico del frontend, como la versión que será utilizada del backend.
     *
     * @function  startModule
     *
     * @param {integer} tipo_modulo - Módulo inicial que sera configurado. 1. Nicho ecológico, 2. Redes de inferencia y 3. Inicio
     */
    function startModule(tipo_modulo) {
        
        _VERBOSE ? console.log("startModule Index") : _VERBOSE;
        
        _url_front = config.url_front
        _url_api = config.url_api
        _url_nicho = config.url_nicho
        _url_comunidad = config.url_comunidad
        _VERBOSE = config.verbose
        _tipo_modulo = 2; // index
        
        
        // Se cargan los archivos de idiomas y depsues son cargados los modulos subsecuentes
        _VERBOSE ? console.log(this) : _VERBOSE
//        _VERBOSE ? console.log("before language_module INDEX") : _VERBOSE;
        _language_module_index = language_module(_VERBOSE);
        _language_module_index.startLanguageModule(this, _tipo_modulo);

    }
    
            
    /**
     * Método de configuración e inicialización de componentes utilizados en la primera vista del sistema.
     *
     * @function _initializeComponents
     * @public
     */
    function _initializeComponents() {

        _VERBOSE ? console.log("_initializeComponents") : _VERBOSE;

        _toastr.options = {
            "debug": false,
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 2000,
            "extendedTimeOut": 2000,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": true,
            "progressBar": true
        };

        $("#link_nicho").prop("href",_url_nicho)
        $("#link_redes").prop("href",_url_comunidad)


        $("#docs_front").prop("href",_url_front+"/frontend-documentation")
        $("#docs_front").text(_url_front+"/frontend-documentation")

        // $("#api_docs").prop("href",_url_front+"/middleware-documentation")
        // $("#api_docs").text(_url_front+"/middleware-documentation")
        $("#api_docs").prop("href","https://documenter.getpostman.com/view/1120638/2s83zjt43S")
        $("#api_docs").text("https://documenter.getpostman.com/view/1120638/2s83zjt43S")


        $("#send_email_login").click(function() {
            _VERBOSE ? console.log("send_email_login") : _VERBOSE;
            _VERBOSE ? console.log("valido: " + $("#email_address")[0].validity["valid"]) : _VERBOSE;

            var regexp = /^(([A-Za-z]+[\-\']?)*([A-Za-z]+)?\s)+([A-Za-z]+[\-\']?)*([A-Za-z]+)?$/;

            if (!regexp.test($("#user_name").val())) {
                _toastr.error(_iTrans.prop("invalid_user"));
                $("#user_name").val("");
                $("#email_address").val("");
                return;
            }

            if ($("#email_address")[0].validity["valid"]) {
                var email = $("#email_address").val();
                var usuario = $("#user_name").val();

                _VERBOSE ? console.log("email: " + email) : _VERBOSE;
                _VERBOSE ? console.log("usuario: " + usuario) : _VERBOSE;

                $.ajax({
                    url: _url_api + "/niche/especie/getUserReg",
                    type: "post",
                    data: {
                        email: email
                    },
                    success: function(d) {
                        var res = d.data;
                        
                        var count = parseInt(res[0].count);
                        _VERBOSE ? console.log("count: " + count) : _VERBOSE;

                        if (count === 0) {
                            $.ajax({
                                url: _url_api + "/niche/especie/setUserReg",
                                type: "post",
                                data: {
                                    email: email,
                                    usuario: usuario
                                },
                                success: function(d) {
                                    _VERBOSE ? console.log("registrado") : _VERBOSE;
                                    _VERBOSE ? console.log(d) : _VERBOSE;
                                    $("#modalLogin").modal("hide");
                                    window.location.replace(_link_val);
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    _VERBOSE ? console.log("error: " + jqXHR) : _VERBOSE;
                                    _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;
                                    _VERBOSE ? console.log("error: " + errorThrown) : _VERBOSE;

                                    $("#email_address").val("");
                                    $("#user_name").val("");

                                    $("#modalLogin").modal("hide");
                                    _toastr.error(_iTrans.prop("general_error"));
                                    // _module_toast.showToast_BottomCenter(_iTrans.prop('lb_correo_error'), "error")
                                }
                            });
                        } else {
                            _VERBOSE ? console.log("Ya registrado") : _VERBOSE;
                            $("#modalLogin").modal("hide");
                            window.location.replace(_link_val);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;

                        $("#email_address").val("");
                        $("#user_name").val("");
                        $("#modalLogin").modal("hide");

                        _toastr.error(_iTrans.prop("general_error"));
                    }
                });

            } else {
                $("#user_name").val("");
                $("#email_address").val("");
                _toastr.error(_iTrans.prop("invalid_user"));
            }
        });




        $("#btn_tutorial").click(function() {
            window.open(_url_front + "/docs/tutorial.pdf");
        });

        var timer = 5000;
        
        var names_nicho = ["lb_index_hist_decil", "lb_index_hist_score", "lb_index_map_pres"];
        var names_net = [ "lb_index_map_riq", "lb_index_tbl_rel", "lb_index_net"];
        
        var index_nicho = 0;
        var index_net = 0;
        $("#lb_ini_nicho").text(_iTrans.prop(names_nicho[names_nicho.length-1]));
        
        setInterval(function() {
            $("#lb_ini_nicho").text(_iTrans.prop(names_nicho[index_nicho]));
            index_nicho = index_nicho === 2 ? 0 : ++index_nicho;
        }, timer);
        
        $("#lb_ini_net").text(_iTrans.prop(names_net[names_net.length-1]));
        setInterval(function() {
            $("#lb_ini_net").text(_iTrans.prop(names_net[index_net]));
            index_net = index_net === 2 ? 0 : ++index_net;
        }, timer);



        $(".box_nicho").bgswitcher({
            images: [
                _url_front + "/images/mapa.png",
                _url_front + "/images/decil.png",
                _url_front + "/images/score_celda.png"],
            effect: "fade",
            interval: timer
        });

        $(".box_net").bgswitcher({
            images: [
                _url_front + "/images/red.png",
                _url_front + "/images/mapa_riqueza.png",
                _url_front + "/images/tabla_red.png"],
            effect: "fade",
            interval: timer
        });

    }


    /**
     * Este método inicializa el módulo de internacionalización del sistema 
     * y la configuración e inicialización de la página de aterrizaje del sistema.
     *
     * @function loadModules
     */
    function loadModules() {
        
        _VERBOSE ? console.log("loadModules INDEX") : _VERBOSE;
        _iTrans = _language_module_index.getI18();
        _initializeComponents();
        
    }

    // retorna solamente un objeto con los miembros que son públicos.
    return {
        startModule: startModule,
        loadModules: loadModules
    };

})();

var modulo = 2
module_index.startModule(modulo)
