
/**
 * Éste módulo es el encargado de controlar la funcionalidad de la página de inicio.
 *
 * @module module_index
 */
var module_index = (function() {
    
    var _VERBOSE = true;
    _VERBOSE ? console.log("*** loading login... ***") : _VERBOSE;

    var _link_val;
    var _auth_module;
    var _url_front;
    var _url_auth;
    var _index_url;

    /**
     * Método encargado de asignar el direccionamiento del ambiente del sistema, 
     * tanto para el ambiente gráfico del frontend, como la versión que será utilizada del backend.
     *
     * @function  startModule
     *
     */
    function startModule() {
        
        _VERBOSE ? console.log("startModule login") : _VERBOSE;

        _auth_module = auth_module();
        _auth_module.startAuthModule();
        _url_front = _auth_module.getFrontUrl();
        _url_auth = _auth_module.getAuthUrl();
        _index_url = _auth_module.getIndexUrl();
        
        $( "#login" ).click(function( event ) {

            console.log("login clicked");

            var data = {
                email:$("#email").val(),
                password:$("#password").val(),
                aviso:true,
                // aviso:$("#aviso").is(":checked"),
                callback: "http://localhost:8080/auth/myCallback"
            }
            var actionUrl = _url_auth + "/auth/login"
            console.log("actionUrl: " + actionUrl);
            
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: data,
                success: function(resp){
                  
                  // console.log(resp)
                  console.log(resp.sessionid)
                  console.log(resp.session.cookie.expires)

                  document.cookie = "sessionid=" + resp.sessionid + "; max-age=" + resp.session.cookie.originalMaxAge +"; path=/";

                  if(resp.status == 0){

                    // console.log("autenticación correcta")
                    window.location.href = _index_url;

                  }
                  else{
                    console.log(resp.message)
                  }
                  

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    
                    $("#errorlb").empty();

                    _VERBOSE ? console.log(jqXHR.responseJSON) : _VERBOSE;
                    
                    if(jqXHR.responseJSON.status == 1){

                      jqXHR.responseJSON.message.forEach(function (error){

                        _VERBOSE ? console.log(error.message) : _VERBOSE;

                        $('#errorlb').append('<p style="color:red;">' + error.message + '</p>');

                      });

                    }

                }
            });

        });


        $( "#registro" ).click(function( event ) {

            console.log("registro");
            $("#errorlb").empty();
            // e.preventDefault(); // avoid to execute the actual submit of the form.

            var data = {
                nombre: $("#nombre").val(),
                email:$("#email").val(),
                procedencia:$("#procedencia").val(),
                password:$("#password").val(),
                // aviso:$("#aviso").is(":checked"),
                aviso:true,
            }
            var actionUrl = _url_auth + "/auth/register"
            
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: data,
                success: function(resp){
                  
                  console.log(resp)
                  
                  if(resp.status == 0){

                    window.location.href = _index_url;

                  }
                  else{
                    console.log(resp.message)
                  }
                  

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    
                    _VERBOSE ? console.log(jqXHR.responseJSON) : _VERBOSE;
                    
                    if(jqXHR.responseJSON.status == 1){

                      jqXHR.responseJSON.message.forEach(function (error){

                        _VERBOSE ? console.log(error.message) : _VERBOSE;

                        $('#errorlb').append('<p style="color:red;">' + error.message + '</p>');

                      });

                    }
                }
            });


        });


        $( "#actualizar" ).click(function( event ) {

            console.log("actualizar");

            $("#errorlb").empty();
            // e.preventDefault(); // avoid to execute the actual submit of the form.

            var data = {
                userid: $("#userid").val(),
                nombre: $("#nombre").val(),
                email:$("#email").val(),
                procedencia:$("#procedencia").val(),
                // password:$("#password").val(),
                // aviso:$("#aviso").is(":checked"),
                aviso:true,
            }
            var actionUrl = _url_auth + "/auth/update"
            
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: data,
                success: function(resp){
                  
                  console.log(resp)
                  
                  if(resp.status == 0){

                    Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Información actualizada',
                      text: 'Información actualizada correctamente',
                      showConfirmButton: true,
                      timer: 5000
                    })

                  }
                  else{
                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        title: 'Error al actualizar Información',
                        text: resp.message,
                        showConfirmButton: true,
                        timer: 5000
                      })
                  }
                  

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    
                    _VERBOSE ? console.log(jqXHR.responseJSON) : _VERBOSE;
                    
                    if(jqXHR.responseJSON.status == 1){

                      Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        title: 'Error al actualizar Información',
                        text: 'Existió un error al actualizar su información, intente mas tarde.',
                        showConfirmButton: true,
                        timer: 5000
                      })

                    }
                }
            });


        });


        $( "#changepassword" ).click(function( event ) {

            console.log("changepassword");

            $("#errorlb").empty();
            // e.preventDefault(); // avoid to execute the actual submit of the form.

            var data = {
                email: $("#email").val(),
                oldpassword: $("#contrasena").val(),
                newpassword:$("#contrasena_nueva").val()
            }
            var actionUrl = _url_auth + "/auth/changePassword"
            
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: data,
                success: function(resp){
                  
                  console.log(resp)

                  if(resp.status == 0){

                    Swal.fire({
                      position: 'center',
                      icon: 'success',
                      title: 'Contraseña actualizada',
                      text: 'Contraseña actualizada correctamente',
                      showConfirmButton: true,
                      timer: 5000
                    })
                  
                  }
                  else{

                    Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        title: 'Error',
                        text: resp.message,
                        showConfirmButton: true,
                        timer: 5000
                      })

                  }

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    
                    _VERBOSE ? console.log(jqXHR.responseJSON) : _VERBOSE;
                    
                    if(jqXHR.responseJSON.status == 1){


                      Swal.fire({
                        position: 'center',
                        icon: 'warning',
                        title: 'Contraseña incorrecta',
                        text: 'Introduzca la contraseña actual correcta',
                        showConfirmButton: true,
                        timer: 5000
                      })

                      

                    }
                }
            });


        });


        


        $( "#recuperar" ).click(function( event ) {

            console.log("recuperar");
            
            $("#messagelb").empty();
            $("#errorlb").empty();

            // TODO: EDITAR ESTA URL, SI ES NECESARIO, A ESTA URL SE AÑADIRA LA URL DEL ARCHIVO DE CAMBIO DE CONTRASEÑA (url_base + /cambio_contrasena.html)
            var url_base = _url_front 
            
            var data = {
                email: $("#email").val(),
                baseurl: url_base
            }
            var actionUrl = _url_auth + "/auth/recoverPassword"
            
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: data,
                success: function(resp){
                  
                  console.log(resp)
                  $('#messagelb').append('<p style="color:green;">Si el correo esta registrado, recibiras una nueva contraseña por ese medio.</p>');
                  
                  // if(resp.status == 0){
                  // }
                  // else{
                  //   console.log(resp.message)
                  //   $('#errorlb').append('<p style="color:red;">' + resp.message + '</p>');
                  // }
                  

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    
                    _VERBOSE ? console.log(jqXHR.responseJSON) : _VERBOSE;
                    
                    if(jqXHR.responseJSON.status == 1){

                      jqXHR.responseJSON.message.forEach(function (error){

                        _VERBOSE ? console.log(error.message) : _VERBOSE;

                        $('#errorlb').append('<p style="color:red;">' + error.message + '</p>');

                      });

                    }
                }
            });


        });


        $( "#cambiarpss" ).click(function( event ) {

            console.log("cambiarpss");
            
            $("#messagelb").empty();
            $("#errorlb").empty();

            var data = {
                email: $("#email").val(),
                oldpassword: $("#oldpassword").val(),
                newpassword: $("#newpassword").val()
            }
            var actionUrl = _url_auth + "/auth/changePassword"
            
            $.ajax({
                type: "POST",
                url: actionUrl,
                data: data,
                success: function(resp){
                  
                  console.log(resp)
                  
                  if(resp.status == 0){
                    $('#messagelb').append('<p style="color:green;">Contraseña modificada correctamente. Ir a Login para acceder.</p>');
                  }
                  else{
                    console.log(resp.message)
                    $('#errorlb').append('<p style="color:red;">' + resp.message + '</p>');
                  }
                  

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    
                    _VERBOSE ? console.log(jqXHR.responseJSON) : _VERBOSE;
                    
                    if(jqXHR.responseJSON.status == 1){

                      jqXHR.responseJSON.message.forEach(function (error){

                        _VERBOSE ? console.log(error.message) : _VERBOSE;

                        $('#errorlb').append('<p style="color:red;">' + error.message + '</p>');

                      });

                    }
                }
            });


        });

    }
    
    // retorna solamente un objeto con los miembros que son públicos.
    return {
        startModule: startModule
    };

})();

module_index.startModule()
