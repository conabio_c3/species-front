
/**
 * Éste módulo es el encargado de controlar la funcionalidad de la página de inicio.
 *
 * @module module_index
 */
var module_index = (function() {
    
    var _VERBOSE = true;

    _VERBOSE ? console.log("*** loading micuenta ... ***") : _VERBOSE;

    var _language_module_index;
    var _toastr = toastr;
    var _iTrans;
    var _tipo_modulo;
    var _link_val;
    var _url_front;
    var _url_api;
    var _url_nicho;
    var _url_comunidad;
    var _utils_module;
    var _url_auth;

    /**
     * Método encargado de asignar el direccionamiento del ambiente del sistema, 
     * tanto para el ambiente gráfico del frontend, como la versión que será utilizada del backend.
     *
     * @function  startModule
     *
     * @param {integer} tipo_modulo - Módulo inicial que sera configurado. 1. Nicho ecológico, 2. Redes de inferencia y 3. Inicio
     */
    function startModule(tipo_modulo) {
        
        _VERBOSE ? console.log("startModule micuenta") : _VERBOSE;
        
        _url_front = config.url_front
        _url_api = config.url_api
        _url_auth = config.url_auth
        _url_nicho = config.url_nicho
        _url_comunidad = config.url_comunidad
        _VERBOSE = config.verbose
        _tipo_modulo = 5; 


        _utils_module = utils_module();
        _utils_module.startUtilsModule();
        
        var actionUrl = _url_auth + "/auth/isAuth"
        console.log("actionUrl: " + actionUrl)

        var session_value = _utils_module.getCookie("sessionid");
        console.log(session_value)
        

        $.ajax({
            type: "POST",
            url: actionUrl,
            data: {
                sessionid: session_value
            },
            success: function(resp){
              
                if(resp.status == 0 ){

                    var session = resp.session[0]
                    console.log(session)

                    var profile = session.sess.user
                    console.log(profile)

                    _userid = profile.userid
                    console.log("User id: " + _userid)


                    actionUrl = _url_api + "/loaddata/getProfileUser"

                    $.ajax({
                        type: "POST",
                        url: actionUrl,
                        data: {
                            userid: _userid
                        },
                        success: function(resp){
                          
                            console.log(resp)

                            if(resp.status == 0 ){

                                console.log(resp.data)

                                uservalues = resp.data[0]


                                $("#nombre").val(uservalues.name)
                                $("#email").val(uservalues.email)
                                $("#procedencia").val(uservalues.procedencia) // no viene en la sesion
                                $("#contrasena").val("*****")
                                $("#userid").val(uservalues.userid)
                                
                            }

                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            
                            _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;
                            console.log("error al obtener lista de datos")
                            
                        }
                    });




                    


                    
                }



            },
            error: function(jqXHR, textStatus, errorThrown) {
                _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;
                console.log("usuario no autenticado")
                window.location.href = _url_front + "/login.html";  
            }
        });


        $("#cclose").click(function(){

            console.log("cclose click")

            var actionUrl = _url_auth + "/auth/closeSesion"

            $.ajax({
                type: "POST",
                url: actionUrl,
                data: {
                    sessionid: session_value
                },
                success: function(resp){

                    console.log(resp)

                    if(resp.status == 0){

                        _utils_module.delete_cookie("sessionid")
                        window.location.href = _url_front + "/login.html";

                    }


                },
                error: function(jqXHR, textStatus, errorThrown) {
                    _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;
                    console.log("no se puede cerrar la sesión ")
                }
            });

        })
        

    }


    
    
            
    /**
     * Método de configuración e inicialización de componentes utilizados en la primera vista del sistema.
     *
     * @function _initializeComponents
     * @public
     */
    function _initializeComponents() {

        _VERBOSE ? console.log("_initializeComponents") : _VERBOSE;

    }


    /**
     * Este método inicializa el módulo de internacionalización del sistema 
     * y la configuración e inicialización de la página de aterrizaje del sistema.
     *
     * @function loadModules
     */
    function loadModules() {
        
        _VERBOSE ? console.log("loadModules Registro") : _VERBOSE;
        
    }

    // retorna solamente un objeto con los miembros que son públicos.
    return {
        startModule: startModule,
        loadModules: loadModules
    };

})();

var modulo = 5
module_index.startModule(modulo)
