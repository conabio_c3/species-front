
// Modulo de apoyo para la autenticación de usuarios en el front
var auth_module = (function () {

    var _userid;
    var _url_front;
    var _url_auth;
    var _index_url;
    var _login_url;
    
    function startAuthModule() {
        
        console.log("startAuthModule")
        
        // inicialización de variables
        _url_front = "http://localhost/species-front";
        // _url_front = "https://species.conabio.gob.mx/dbdev";
        
        _url_auth = "http://localhost:8081";
        // _url_auth = "https://species.conabio.gob.mx/api/auth";

        // Configurar a donde redirecciona en caso de ser una autenticación exitosa
        _index_url = _url_front + "/micuenta.html";
        
        // Configurar a donde redirecciona en caso de ser una autenticación erronea
        _login_url = _url_front + "/login.html";


    }

    function getFrontUrl(){
        return _url_front;
    }

    function getAuthUrl(){
        return _url_auth;
    }

    function getIndexUrl(){
        return _index_url;
    }

    function getUserId(){
        return _userid;
    }

    async function validaAcceso(callback){

        console.log("validaAcceso")

        var actionUrl = _url_auth + "/auth/isAuth"

        var session_value = getCookie("sessionid");
        console.log(session_value)

        if(session_value == "" || session_value == undefined){
            console.log("id de sesión no encontrado")
            window.location.href = _login_url;  
        }

        $.ajax({
            type: "POST",
            url: actionUrl,
            async: false,
            data: {
                sessionid: session_value
            },
            success: function(resp){
              
                var session = resp.session[0]
                // console.log(session)

                if(resp.status == 0 ){

                    console.log("usuario autenticado")

                    var profile = session.sess.user
                    // console.log(profile)
                    // _userid = profile.userid
                    // console.log("User id: " + _userid)
                    callback(profile)

                }
                else{
                    console.log("usuario no autenticado")
                    window.location.href = _login_url;
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                _VERBOSE ? console.log("error: " + textStatus) : _VERBOSE;
                console.log("usuario no autenticado")
                window.location.href = _login_url;
            }
        });

    }

    function getCookie(cname) {

        console.log("getCookie")

        let name = cname + "=";
        let ca = document.cookie.split(';');
        
        for(let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }

        return "";

    }

    return{
        startAuthModule: startAuthModule,
        validaAcceso: validaAcceso,
        getFrontUrl: getFrontUrl,
        getAuthUrl: getAuthUrl,
        getIndexUrl: getIndexUrl,
        getUserId: getUserId
    }

});

